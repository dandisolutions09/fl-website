import React from "react";
import Snackbar from "@mui/material/Snackbar";
import Alert from "@mui/material/Alert";

//const AdminAddedSuccess = ({ open, onClose, autoHideDuration, name, mssg }) => {

// const Success = ({ open, onClose, autoHideDuration }) => {
const GeneralSuccess = ({ open, onClose, autoHideDuration, msg }) => {
  return (
    <Snackbar
      open={open}
      autoHideDuration={autoHideDuration}
      onClose={onClose}
      anchorOrigin={{ vertical: "top", horizontal: "center" }}
    >
      <Alert
        onClose={onClose}
        severity="success"
        sx={{ width: ["60%", "100%"] }}
      >
        {msg}
      </Alert>
    </Snackbar>
  );
};

export default GeneralSuccess;
