

import React from "react";
import ios from "../assets/ios.png";
import android from "../assets/android.png";

import image1 from "../assets/imagesforwebsite1/6.png";
import image2 from "../assets/imagesforwebsite1/12.png";
import image3 from "../assets/imagesforwebsite1/planet.jpg";

const BenefitsSection = () => {
  return (
    <div className="bg-gray-100 py-16 px-6 sm:px-6 lg:px-12 xl:px-24 mt-4 rounded-[50px] md:rounded-[800px]">
      <div className="max-w-7xl mx-auto text-center">
        <h2 className="text-3xl sm:text-4xl md:text-5xl lg:text-6xl font-extrabold text-green-800">
          Why Fine Foods App
        </h2>
      </div>

      <div className="mt-10 grid gap-10 md:grid-rows-3 max-w-7xl mx-auto relative">
        {/* First Card */}
        <div className="flex flex-col md:flex-row items-center text-center bg-grey p-6 rounded-lg relative z-10">
          <div className="w-[250px] h-[250px] sm:w-[350px] sm:h-[350px] md:w-[450px] md:h-[450px] bg-gradient-to-r rounded-full flex items-center justify-center relative">
            {/* Image Placeholder */}
            <img
              src={image1}
              alt="Give and get items"
              className="w-full h-full object-cover rounded-full"
              style={{ position: "relative", zIndex: 10, marginTop: "60px" }}
            />
          </div>

          <div className="mt-6 md:mt-0 md:ml-10 w-full md:w-[460px]">
            <h3 className="text-xl sm:text-2xl md:text-3xl lg:text-4xl text-[#00563B]">
              1. Enjoy Good Food at an Affordable Price
            </h3>
            <p className="text-gray-500 text-sm sm:text-base md:text-lg">
              Get surplus food from restaurants, cafes, and stores at
              significantly discounted prices.
            </p>
          </div>
        </div>

        {/* Second Card */}
        <div className="flex flex-col-reverse md:flex-row items-center text-center bg-grey p-6 rounded-lg relative z-0">
          <div className="mt-6 md:mt-0 md:mr-10 w-full md:w-[460px]">
            <h3 className="text-xl sm:text-2xl md:text-3xl lg:text-4xl text-[#00563B]">
              2. Discover New Places
            </h3>
            <p className="text-gray-500 text-sm sm:text-base md:text-lg">
              Try new local food establishments.
            </p>
          </div>

          <div className="w-[250px] h-[250px] sm:w-[350px] sm:h-[350px] md:w-[450px] md:h-[450px] bg-gradient-to-r flex items-center justify-center">
            {/* Image Placeholder */}
            <img
              src={image2}
              alt="Discover new places"
              className="w-full h-full rounded-full"
              style={{ position: "relative", zIndex: -1 }}
            />
          </div>
        </div>

        {/* Third Card */}
        <div className="flex flex-col md:flex-row items-center text-center bg-grey p-6 rounded-lg relative z-10">
          <div className="w-[250px] h-[250px] sm:w-[350px] sm:h-[350px] md:w-[450px] md:h-[450px] bg-gradient-to-r rounded-full flex items-center justify-center relative">
            {/* Image Placeholder */}
            <img
              src={image3}
              alt="Save the planet"
              className="w-full h-full object-cover rounded-full"
              style={{ position: "relative", zIndex: 10, marginTop: "-60px" }}
            />
          </div>

          <div className="mt-6 md:mt-0 md:ml-10 w-full md:w-[460px]">
            <h3 className="text-xl sm:text-2xl md:text-3xl lg:text-4xl text-[#00563B]">
              3. Save the Planet
            </h3>
            <p className="text-gray-500 text-sm sm:text-base md:text-lg">
              You are actively contributing to reducing food waste and its
              environmental effects.
            </p>
          </div>
        </div>
      </div>
    </div>
  );
};

export default BenefitsSection;
