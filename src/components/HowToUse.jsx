import React from "react";

// import phoneScreen from "../assets/imagesforwebsite/iphone-background.png";

import phoneScreen2 from "../assets/imagesforwebsite1/phone_mockups/3_mock.png";




// import image2 from "../assets/imagesforwebsite/12.png";

// import image3 from "../assets/imagesforwebsite/planet.jpg";

const HowToComp = () => {
  return (
    <div className="flex flex-col gap-[120px] justify-center items-center p-12 bg-gradient-to-br from-green-800 to-orange-200 min-h-screen rounded-[10px] mt-[-300px] ">
      <h className="text-6xl font-bold text-white-700">How To Use The App</h>

      <div>
        <div className="flex max-w-6xl w-full">
          {/* Text Section */}
          <div className="flex-1 p-8">
            <h1 className="text-5xl font-bold text-purple-900 mb-6">Snap 1</h1>
            <p className="text-lg text-gray-700">
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
              eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
              enim ad minim veniam, quis nostrud exercitation ullamco laboris
              nisi ut aliquip ex ea commodo consequat.
            </p>
          </div>

          {/* Image Section */}
          <div className="flex-1 flex justify-center items-center">
            <img
              src={phoneScreen2} // Add your mockup image path here
              alt="App mockup"
              className="max-w h-auto rounded-3xl shadow-lg"
            />
          </div>
        </div>
      </div>

      <div>
        <div className="flex max-w-6xl w-full">
          {/* Text Section */}
          <div className="flex-1 p-8">
            <h1 className="text-5xl font-bold text-purple-900 mb-6">Snap 2</h1>
            <p className="text-lg text-gray-700">
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
              eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
              enim ad minim veniam, quis nostrud exercitation ullamco laboris
              nisi ut aliquip ex ea commodo consequat.
            </p>
          </div>

          {/* Image Section */}
          <div className="flex-1 flex justify-center items-center">
            <img
              src={phoneScreen2} // Add your mockup image path here
              alt="App mockup"
              className="max-w-full h-auto rounded-3xl shadow-lg"
            />
          </div>
        </div>
      </div>

      <div>
        <div className="flex max-w-6xl w-full">
          {/* Text Section */}
          <div className="flex-1 p-8">
            <h1 className="text-5xl font-bold text-purple-900 mb-6">Snap 3</h1>
            <p className="text-lg text-gray-700">
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
              eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
              enim ad minim veniam, quis nostrud exercitation ullamco laboris
              nisi ut aliquip ex ea commodo consequat.
            </p>
          </div>

          {/* Image Section */}
          <div className="flex-1 flex justify-center items-center">
            <img
              src={phoneScreen2} // Add your mockup image path here
              alt="App mockup"
              className="max-w-full h-auto rounded-3xl shadow-lg"
            />
          </div>
        </div>
      </div>
    </div>
  );
};

export default HowToComp;
