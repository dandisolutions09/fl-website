import mainImage0 from "../assets/imagesforwebsite/10.png";
import iphone from "../assets/imagesforwebsite/iPhone_mockup_final.png";
// import mainImage from "../assets/imagesforwebsite/phone_mockups/file.png";
// import mainImage1 from "../assets/imagesforwebsite/0.png";
import slide1 from "../assets/imagesforwebsite/1_1.png";
import slide2 from "../assets/imagesforwebsite/2_2.png";
import slide3 from "../assets/imagesforwebsite/3_3.png";
import slide4 from "../assets/imagesforwebsite/4_4.png";
import slide5 from "../assets/imagesforwebsite/5_5.png";
import slide6 from "../assets/imagesforwebsite/6_6.png";
import slide7 from "../assets/imagesforwebsite/7_7.png";
import slide8 from "../assets/imagesforwebsite/8_8.png";
import slide9 from "../assets/imagesforwebsite/10.png";
// import slide10 from "../assets/imagesforwebsite/11.png";
// import mainImage12 from "../assets/imagesforwebsite/15.png";
// import mainImage13 from "../assets/imagesforwebsite/16.png";
// import mainImage14 from "../assets/imagesforwebsite/17.png";

import ios from "../assets/ios.png";
import android from "../assets/android.png";
import { motion } from "framer-motion";

import React from "react";
import Navbar from "../components/Navbar";
import BenefitsSection from "../components/Benefits";
import HowToComp from "../components/HowToUse";
import ImageSlider from "./ImageSlider";

const HomePage = () => {
  const slides = [
    // {
    //   url: mainImage1,
    //   title: [
    //     { label1: "SETUP RUST SERVER..." },
    //     { label2: "X BOX SERVERS" },
    //     { label3: "Survive the harsh wilderness on our Rust server" },
    //   ],
    //   // buttons: [{ label: "RENT RUST", onClick: handleRentRustClick }, { label: "READ MORE" }],
    //   // buttons: [
    //   //   { label: "GET STARTED", onClick: handleRentRustClick },
    //   //   { label: "READ MORE", onClick: handleReadMoreClick },
    //   // ],
    // },
    {
      url: slide1,
      title: [
        {
          label1:
            "Attract new customers while promoting less food waste on the Fine Foods app.",
        },
        // { label2: "Space Engineers servers are now available." },
        // {
        //   label3: "Embark on a thrilling journey in our Space Engineers server",
        // },
      ],

      buttons: [{ label: "RENT SERVER" }, { label: "READ MORE" }],

      leftPercentage: "50%",
    },

    {
      url: slide2,
      title: [
        // { label1: "Creating a sustainable future and an eco-friendly world for generations to come." },
        // { label2: "Get your servers up and running" },
        // {
        //   label3:
        //     "Join our Minecraft server for an epic survival adventure with custom plugins, friendly community, and exciting events!",
        // },
      ],

      buttons: [{ label: "RENT SERVER" }, { label: "READ MORE" }],
    },

    {
      url: slide3,
      title: [
        {
          label1:
            "Creating a sustainable future and an eco-friendly world for generations to come.",
        },
        // { label2: "Get your servers up and running" },
        // {
        //   label3:
        //     "Join our Minecraft server for an epic survival adventure with custom plugins, friendly community, and exciting events!",
        // },
      ],

      buttons: [{ label: "RENT SERVER" }, { label: "READ MORE" }],

      leftPercentage: "5%",
    },

    {
      url: slide4,
      title: [
        {
          label1: " Discover delicious fine foods at extremely low cost.",
        },
        // { label2: "Get your servers up and running" },
        // {
        //   label3:
        //     "Join our Minecraft server for an epic survival adventure with custom plugins, friendly community, and exciting events!",
        // },
      ],

      buttons: [{ label: "RENT SERVER" }, { label: "READ MORE" }],
      leftPercentage: "5%",
    },

    {
      url: slide5,
      title: [
        {
          label1:
            "We help businesses create revenue from surplus food while creating a greener sustainable world.",
        },
        // { label2: "Get your servers up and running" },
        // {
        //   label3:
        //     "Join our Minecraft server for an epic survival adventure with custom plugins, friendly community, and exciting events!",
        // },
      ],

      buttons: [{ label: "RENT SERVER" }, { label: "READ MORE" }],
      leftPercentage: "5%",
    },

    {
      url: slide6,
      title: [
        {
          label1:
            "By 2030, we aim to make Saudi Arabia a leader in food sustainability and advocates of a greener, eco-friendly planet.",
        },
        // { label2: "Get your servers up and running" },
        // {
        //   label3:
        //     "Join our Minecraft server for an epic survival adventure with custom plugins, friendly community, and exciting events!",
        // },
      ],

      buttons: [{ label: "RENT SERVER" }, { label: "READ MORE" }],
      leftPercentage: "5%",
    },

    {
      url: slide7,
      title: [
        {
          label1:
            "Saving good food from being wasted, while bringing new customers to your door.",
        },
        // { label2: "Get your servers up and running" },
        // {
        //   label3:
        //     "Join our Minecraft server for an epic survival adventure with custom plugins, friendly community, and exciting events!",
        // },
      ],

      buttons: [{ label: "RENT SERVER" }, { label: "READ MORE" }],
      leftPercentage: "5%",
    },

    {
      url: slide8,
      title: [
        { label1: "A win-win-win for consumers, businesses and the planet!" },
        // { label2: "Get your servers up and running" },
        // {
        //   label3:
        //     "Join our Minecraft server for an epic survival adventure with custom plugins, friendly community, and exciting events!",
        // },
      ],

      buttons: [{ label: "RENT SERVER" }, { label: "READ MORE" }],
      leftPercentage: "5%",
    },

    // {
    //   url: mainImage10,
    //   title: [
    //     { label1: "MINECRAFT SERVERS" },
    //     { label2: "Get your servers up and running" },
    //     {
    //       label3:
    //         "Join our Minecraft server for an epic survival adventure with custom plugins, friendly community, and exciting events!",
    //     },
    //   ],

    //   buttons: [{ label: "RENT SERVER" }, { label: "READ MORE" }],
    // },

    // {
    //   url: mainImage11,
    //   title: [
    //     { label1: "MINECRAFT SERVERS" },
    //     { label2: "Get your servers up and running" },
    //     {
    //       label3:
    //         "Join our Minecraft server for an epic survival adventure with custom plugins, friendly community, and exciting events!",
    //     },
    //   ],

    //   buttons: [{ label: "RENT SERVER" }, { label: "READ MORE" }],
    // },

    // {
    //   url: mainImage12,
    //   title: [
    //     { label1: "MINECRAFT SERVERS" },
    //     { label2: "Get your servers up and running" },
    //     {
    //       label3:
    //         "Join our Minecraft server for an epic survival adventure with custom plugins, friendly community, and exciting events!",
    //     },
    //   ],

    //   buttons: [{ label: "RENT SERVER" }, { label: "READ MORE" }],
    // },

    // {
    //   url: mainImage13,
    //   title: [
    //     { label1: "MINECRAFT SERVERS" },
    //     { label2: "Get your servers up and running" },
    //     {
    //       label3:
    //         "Join our Minecraft server for an epic survival adventure with custom plugins, friendly community, and exciting events!",
    //     },
    //   ],

    //   buttons: [{ label: "RENT SERVER" }, { label: "READ MORE" }],
    // },

    // {
    //   url: mainImage14,
    //   title: [
    //     { label1: "MINECRAFT SERVERS" },
    //     { label2: "Get your servers up and running" },
    //     {
    //       label3:
    //         "Join our Minecraft server for an epic survival adventure with custom plugins, friendly community, and exciting events!",
    //     },
    //   ],

    //   buttons: [{ label: "RENT SERVER" }, { label: "READ MORE" }],
    // },
  ];

  const containerStyles = {
    width: "100%", // Changed "full" to "100%"
    height: "750px",
    marginTop: "100px",
  };

  return (
    <>
      <div className="bg-[#00563B]">
        <header className="flex flex-col md:flex-row justify-between items-center bg-[#01A437] py-2 px-4 h-auto  md:h-6">
          <select className="text-sm bg-transparent focus:outline-none">
            <option>English</option>
            <option>Arabic</option>
          </select>
          <a href="#" className="text-white text-xs mt-2 md:mt-0">
            <strong className="text-white">
              Surplus food available on The Fine Foods app every day –
            </strong>{" "}
            download it and pick up your meal today!
          </a>
        </header>

        {/* Sticky Navbar Wrapper */}
        <div className="sticky top-0 z-50 bg-[#00563B]">
          <Navbar />
        </div>
        <div className="relative w-full min-h-[400px] sm:min-h-[600px] md:min-h-[800px]">
          {/* Background Image */}
          <img
            src={mainImage0}
            alt="Background"
            className="absolute inset-0 w-full h-full object-cover"
          />

          {/* Gradient/Hue at the Bottom */}
          <div className="absolute inset-x-0 bottom-0 h-1/4 bg-gradient-to-t from-black to-transparent opacity-100"></div>

          {/* Text Overlay */}
          <div className="absolute bottom-[20px] left-5 sm:bottom-18 sm:left-8 text-left">
            <h1 className="text-xl md:text-8xl text-white font-bold">
              Sustain Food, <br />
              <span className="block text-[#01A437]">Sustain Earth.</span>
            </h1>

            <p className="mt-4 text-xs md:text-3xl text-white p-4 font-bold">
              At Fine Foods, our mission is to combat food waste by helping
              businesses generate revenue from surplus food.
            </p>
          </div>

          {/* Overlay Image */}
          <img
            src={iphone}
            alt="Overlay"
            className="absolute right-0 top-[-40px] h-[80%] sm:h-[90%] md:h-full w-auto object-cover opacity-90"
          />
        </div>

        {/* <div>
          <h1 className="text-4xl md:text-8xl text-white font-bold">
            Sustain Food, <span className="text-[#01A437]">Sustain Earth.</span>
          </h1>

          <p className="mt-4 text-lg md:text-xl text-white p-4">
            At Fine Foods, our mission is to combat food waste by helping
            businesses generate revenue from surplus food.
          </p>
        </div> */}

        {/* <div className="flex justify-center items-center h-screen bg-[#00563B]">
          <div className="h-[600px] w-[100%] md:w-[1000px] bg-black">
            <ImageSlider slides={slides} />
          </div>
        </div> */}

        {/* const containerStyles = {
    width: "100%", // Changed "full" to "100%"
    height: "750px",
    marginTop: "100px",
  }; */}

        <div className="mt-2 h-[400px] md:h-[600px] lg:h-[800px]">
          <ImageSlider slides={slides} />
        </div>

        <div>
          <BenefitsSection />
        </div>
      </div>
    </>
  );
};

export default HomePage;
