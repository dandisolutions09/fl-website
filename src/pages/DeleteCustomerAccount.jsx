import React, { useState } from "react";
import Box from "@mui/material/Box";
import TextField from "@mui/material/TextField";
import Button from "@mui/material/Button";
import Navbar from "../components/Navbar";
import GeneralError from "../components/GeneralError";
import GeneralSuccess from "../components/GeneralSuccess";
import axios from "axios";

// import GeneralError from "../components/Alerts-2/GeneralError";
// import GeneralSuccess from "../components/Alerts-2/GeneralSuccess";

export default function DeleteCustomerAccount() {
  const [formData, setFormData] = useState({
    // search: "",
    quantity: "",
    //password: "",
  });

  const [generalError, setGeneralError] = useState();
  const [generalSuccess, setGeneralSuccess] = useState();

  const [errorMsg, setErrorMsg] = useState();
  const [successMsg, setSuccessMsg] = useState();

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormData({
      ...formData,
      [name]: value,
    });
  };

  const handlePostRequest = async (data_to_be_sent) => {
    // Prepare the data to be sent in the POST request
    const data = {
      phone_number: data_to_be_sent.phone_number,
      email: data_to_be_sent.email,
    };

    // Define the URL for the API endpoint
    // const url = "https://api.finefoodsapp.com/delete-partner-from-website"; // Replace with the actual endpoint URL

    //const url = "http://localhost:8080/delete-partner-from-website"; // console.log("current gastank id", data_to_be_sent.gasTankID);
    const url = "https://api.finefoodsapp.com/delete-customer-from-website";
    try {
      // Sending POST request with the data
      const response = await axios.post(
        // endpoints.newGastTankTransactions,
        // data        `${endpoints.removeServiceFromCart}?id=${selectedCustomerProp._id}&cart_id=${itemIdProp}&service_id=${serviceId}`,
        `${url}?email=${data_to_be_sent.email}&phone_number=${data_to_be_sent.phone_number}`
      );

      // Handle the response
      console.log("Data posted successfully:", response.data);

      // alert("Successfully Updated Tank Record");
      setSuccessMsg(response.data.message);
      setGeneralSuccess(true);

      // setTimeout(() => {
      //   //setOpenAddCompany(false);
      //   window.location.reload();
      // }, 1000);
    } catch (error) {
      // Handle any errors
      console.error("Error posting data:", error.data);
      //setError(error.response.data);

      setErrorMsg(error.response.data);
      setGeneralError(true);
    }
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    console.log("Form submitted:", formData);
    // console.log("selected row data", selectedRecord);

    // var transactionType;

    // if (storedRoleObject == "Porter") {
    //   transactionType = "subtraction";
    // } else if (storedRoleObject == "Pharmacist") {
    //   transactionType = "addition";
    // }
    const data = {
      //gasTankID: selectedRecord._id,
      phone_number: formData.phone_number,
      email: formData.email,
    };

    console.log("data to be sent", data);

    handlePostRequest(data);
    // Add form submission logic here
  };
  return (
    <>
      <GeneralSuccess
        open={generalSuccess}
        autoHideDuration={6000}
        //name={"Success"}
        msg={successMsg}
        onClose={() => setGeneralSuccess(false)}
      />

      <GeneralError
        open={generalError}
        autoHideDuration={6000}
        Errmsg={errorMsg}
        onClose={() => setGeneralError(false)}
      />


      <p class="text-center text-2xl font-bold mt-8">Delete Customer Account</p>
      <div className="max-w-md mx-auto mt-4 p-6 bg-white rounded-lg shadow-md mb-[445px]">
        {/* Search Field */}

        {/* Form */}
        <form onSubmit={handleSubmit} className="space-y-4">
          {/* Name Field */}

          {/* {error && (
            <div className="text-red-600 text-sm mt-2">
              <p>Error: {error}</p>
            </div>
          )} */}

          <div className="">
            <label
              htmlFor="name"
              className="block text-sm font-medium text-gray-700 mb-1 mt-4"
            >
              Email
            </label>
            <input
              type="text"
              id="email"
              name="email"
              placeholder="Enter Email"
              value={formData.name}
              onChange={handleChange}
              required
              className="w-full mb-8 px-4 py-2 border border-gray-300 rounded-md focus:outline-none focus:ring-2 focus:ring-blue-500 focus:border-blue-500"
            />
            <label
              htmlFor="name"
              className="block text-sm font-medium text-gray-700 mb-1"
            >
              Phone Number
            </label>
            <input
              type="text"
              id="phone_number"
              name="phone_number"
              placeholder="Enter Phone Number"
              value={formData.name}
              onChange={handleChange}
              required
              className="w-full mb-4 px-4 py-2 border border-gray-300 rounded-md focus:outline-none focus:ring-2 focus:ring-blue-500 focus:border-blue-500"
            />
          </div>

          {/* Submit Button */}
          {/* <button
    type="submit"
    className="w-full bg-blue-500 text-white font-semibold py-2 px-4 rounded-md hover:bg-blue-600 transition"
  >
    Submit
  </button> */}

          <Button
            type="submit"
            className={`w-full text-white font-semibold py-2 px-4 rounded-md transition `}
            color="success"
            variant="contained"
            // disabled={
            //   storedRoleObject === "Porter" && selectedRecord.qty_balance === 0
            // } // Disable button when role is Porter and qty is 0
          >
            Submit
          </Button>

          {/* <button
    type="submit"
    className="w-full bg-blue-500 text-white font-semibold py-2 px-4 rounded-md hover:bg-blue-600 transition"
    disabled={storedRoleObject === "Porter" && selectedRecord.qty_balance === 0} // Disable button when role is Porter and qty is 0
    // Disable the button if qty is 0
  >
    {storedRoleObject === "Porter"
      ? "Remove Tank"
      : storedRoleObject === "Pharmacist"
      ? "Add Tank"
      : "Submit"}
  </button> */}
        </form>
      </div>
    </>
  );
}
