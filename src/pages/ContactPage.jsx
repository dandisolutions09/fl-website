import React from "react";
import Box from "@mui/material/Box";
import TextField from "@mui/material/TextField";
import Button from "@mui/material/Button";
import Navbar from "../components/Navbar";

export default function ContactPage() {
  return (
    <>
       <div className="sticky top-0 z-50 bg-[#00563B]">
          <Navbar />
        </div>
      <div className="min-h-screen flex flex-col justify-center items-center mt-[50px] sm:mt-[100px]">
        <h1 className="text-3xl sm:text-4xl lg:text-5xl underline my-3 text-center">
          Contact Us
        </h1>
        <p className="px-6 sm:px-12 lg:px-[200px] font-thin text-center text-sm sm:text-base lg:text-lg">
          Welcome to Fine Foods, the easiest way to get around at the tap of a
          button.
        </p>

        <div className="w-full sm:w-3/4 bg-[#f2dede] p-4 rounded my-4"></div>

        <div className="w-full px-6 sm:px-12 lg:w-3/4 flex flex-col gap-6 mt-6">
          <div className="flex flex-col sm:flex-row gap-3">
            <TextField
              id="outlined-basic"
              label="First Name"
              variant="outlined"
              sx={{ width: "100%" }}
            />
            <TextField
              id="outlined-basic"
              label="Second Name"
              variant="outlined"
              sx={{ width: "100%" }}
            />
          </div>

          <div className="flex flex-col sm:flex-row gap-3">
            <TextField
              id="outlined-basic"
              label="First Name"
              variant="outlined"
              sx={{ width: "100%" }}
            />
            <TextField
              id="outlined-basic"
              label="Email"
              variant="outlined"
              sx={{ width: "100%" }}
            />
          </div>

          <div className="flex flex-col sm:flex-row gap-3">
            <TextField
              id="outlined-basic"
              label="Phone"
              variant="outlined"
              sx={{ width: "100%" }}
            />
            <TextField
              id="outlined-basic"
              label="Details"
              variant="outlined"
              placeholder="Please write details"
              sx={{ width: "100%" }}
            />
          </div>

          <div className="flex justify-center sm:justify-end mt-6">
            <Button variant="contained" sx={{ bgcolor: "#000" }}>
              Submit
            </Button>
          </div>
        </div>
      </div>
    </>
  );
}
