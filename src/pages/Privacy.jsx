import Navbar from "../components/Navbar";

const PrivacyPol = () => {
  return (
    <>

<div className="sticky top-0 z-50 bg-[#00563B]">
          <Navbar />
        </div>
      <div className="p-[50px] md:p-[80px]">
        {/* <h1 className="text-3xl sm:text-2xl md:text-3xl  my-3 ">Cookie Policy</h1> */}

        <h1 className="text-5xl  my-3 text-center mb-4"> Privacy Policy</h1>

        <h1 className="font-bold my-4">Effective Date: 7/10/2024</h1>
        <p>
          At Fine Foods, we prioritize your privacy and are committed to
          protecting your personal information. Our mission is to revolutionize
          food sustainability by offering surplus food at reduced prices and
          helping businesses turn surplus into revenue. This Privacy Policy
          explains how we collect, use, disclose, and safeguard your personal
          information when you use the Fine Foods app and services. By accessing
          or using our app, you agree to the practices outlined in this Privacy
          Policy.
        </p>

        <h2 className="text-3xl sm:text-2xl md:text-3xl  my-3 ">
          1. Information We Collect
        </h2>
        <p>
          We collect several types of information from and about users of our
          app, including:
        </p>

        <h2 className="text-2xl sm:text-2xl md:text-2xl  my-3 ">
          1.1 Personal Identifiable Information (PII):
        </h2>
        <ul className="list-disc ml-6">
          <li>
            Contact Information: Your name, email address, phone number, and
            other contact details.
          </li>
          <li>
            <strong>Account Information:</strong> Username, password, and any
            profile data you choose to provide.
          </li>
          <li>
            <strong>Payment Information: </strong> Credit card details, billing
            address, and other financial data required to complete transactions.
            (Note: We use secure third-party payment processors and do not store
            sensitive payment information directly on our servers.)
          </li>
        </ul>

        <h2 className="text-2xl sm:text-2xl md:text-2xl  my-3 ">
          1.2 Location Data:
        </h2>

        <ul className="list-disc ml-6">
          <li>
            With your permission, we collect and process information about your
            location to connect you with nearby participating restaurants and
            stores offering surplus food. Location data may include GPS data,
            Wi-Fi access points, and cellular tower proximity. You can control
            location data permissions through your device settings.
          </li>
        </ul>

        <h2 className="text-2xl sm:text-2xl md:text-2xl  my-3 ">
          1.3 Usage and Analytics Data:
        </h2>

        <ul className="list-disc ml-6">
          <li>
            We collect data on how you interact with our app, including the
            pages you visit, the features you use, the time spent on the app, IP
            addresses, and error reports to help diagnose issues and improve the
            overall experience. This also includes anonymized data used for
            analytical purposes to help us better understand user preferences
            and behaviors.
          </li>
        </ul>

        <h2 className="text-2xl sm:text-2xl md:text-2xl  my-3 ">
          1.4 Device Information:
        </h2>

        <ul className="list-disc ml-6">
          <li>
            We automatically collect technical information from your device,
            such as device type, operating system version, unique device
            identifiers (e.g., IP address), and mobile network information to
            ensure optimal compatibility and performance of the app.
          </li>
        </ul>

        <h2 className="text-3xl sm:text-2xl md:text-3xl  my-3 ">
          2. How We Use Your Information
        </h2>

        <p>
          We use the information we collect in various ways to provide,
          maintain, and improve the Fine Foods app, ensuring an efficient and
          user-friendly experience. Our purposes include, but are not limited
          to:
        </p>

        <h2 className="text-2xl sm:text-2xl md:text-2xl  my-3 ">
          2.1 Providing and Improving Our Services:
        </h2>

        <ul className="list-disc ml-6">
          <li>
            Your personal information helps us operate and improve the app,
            process your transactions, and connect you with businesses offering
            Treat Bags (surplus food). We also use the data to tailor your
            experience, providing recommendations based on your preferences and
            usage history.
          </li>
        </ul>

        <h2 className="text-2xl sm:text-2xl md:text-2xl  my-3 ">
          2.2 Communication:
        </h2>

        <ul className="list-disc ml-6">
          <li>
            We may use your contact details to send important notices about
            updates to our services, app changes, and terms of use. We may also
            send you promotional offers, discounts, or surveys to enhance your
            experience with Fine Foods. You may opt out of promotional emails at
            any time by following the unsubscribe instructions included in these
            communications.
          </li>
        </ul>

        <h2 className="text-2xl sm:text-2xl md:text-2xl  my-3 ">
          2.3 Facilitating Transactions:
        </h2>

        <ul className="list-disc ml-6">
          <li>
            We use your payment information to process purchases, refunds, and
            order confirmations securely. Our payment partners are PCI-compliant
            and ensure your financial information is handled in the safest
            manner possible.
          </li>
        </ul>

        <h2 className="text-2xl sm:text-2xl md:text-2xl  my-3 ">
          2.4 Personalization:
        </h2>

        <ul className="list-disc ml-6">
          <li>
            Fine Foods leverages analytics to personalize your user experience.
            For example, we may recommend Treat Bags based on your location,
            previous orders, or user preferences.
          </li>
        </ul>

        <h2 className="text-2xl sm:text-2xl md:text-2xl  my-3 ">
          2.5 Security and Fraud Prevention::
        </h2>

        <ul className="list-disc ml-6">
          <li>
            To protect the security of your account, we use the information to
            authenticate users, monitor account activity, detect suspicious
            activities, and prevent fraud or other illegal activities.
          </li>
        </ul>

        <h2 className="text-3xl sm:text-2xl md:text-3xl  my-3 ">
          3.How We Share Your Information
        </h2>

        <p>
          We may share your personal data with third parties only in the ways
          described in this Privacy Policy:
        </p>

        <h2 className="text-2xl sm:text-2xl md:text-2xl  my-3 ">
          3.1 Service Providers:
        </h2>

        <ul className="list-disc ml-6">
          <li>
            We work with trusted third-party service providers who assist in
            delivering our services, including hosting services, payment
            processors, analytics tools, marketing partners, and customer
            support platforms. These providers only receive the necessary data
            to perform their services and are contractually obligated to protect
            your data and refrain from using it for other purposes.
          </li>
        </ul>

        <h2 className="text-2xl sm:text-2xl md:text-2xl  my-3 ">
          3.2 Business Transactions:
        </h2>

        <ul className="list-disc ml-6">
          <li>
            In the event of a merger, acquisition, sale of assets, or other
            business transaction, we may transfer your personal data to the
            relevant party involved, but your information will remain subject to
            the protections outlined in this Privacy Policy.
          </li>
        </ul>

        <h2 className="text-2xl sm:text-2xl md:text-2xl  my-3 ">
          3.3 Legal Requirements and Protection of Rights:
        </h2>

        <ul className="list-disc ml-6">
          <li>
            We may disclose your information if required by law or if we believe
            it is necessary to:
            <ul className="list-disc ml-6">
              <li>
                Comply with legal processes, such as a subpoena or court order.
              </li>

              <li>
                Protect the rights, property, or safety of Fine Foods, its
                users, or the public.
              </li>

              <li>
                Prevent or address fraud, security breaches, or other harmful
                activities.
              </li>
            </ul>
          </li>
        </ul>

        <h2 className="text-3xl sm:text-2xl md:text-3xl  my-3 ">
          4. Data Security
        </h2>

        <p>
          We are committed to protecting your personal data. Fine Foods
          implements various physical, electronic, and administrative measures
          to safeguard your information from unauthorized access, misuse, or
          disclosure. Our efforts include encryption protocols, secure data
          storage, access control measures, and continuous security assessments.
        </p>

        <p className="mt-2">
          However, no method of transmission over the internet or method of
          electronic storage is 100% secure. While we strive to protect your
          personal data, we cannot guarantee its absolute security.
        </p>

        <h2 className="text-3xl sm:text-2xl md:text-3xl  my-3 ">
          5. Third-Party Links
        </h2>

        <p>
          Our app may contain links to external websites, services, or apps that
          are not operated by Fine Foods. Please be aware that this Privacy
          Policy does not apply to such third-party platforms. We recommend that
          you review the privacy policies of these third-party websites or
          services before engaging with them, as Fine Foods is not responsible
          for their privacy practices.
        </p>

        <h2 className="text-3xl sm:text-2xl md:text-3xl  my-3 ">
          6. Data Retention and Deletion
        </h2>

        <p>
          We retain your personal data only for as long as necessary to fulfill
          the purposes outlined in this Privacy Policy, unless a longer
          retention period is required by law. Once your data is no longer
          needed, we securely delete or anonymize it.
        </p>

        <p>
          If you wish to delete your account or request that we delete your
          personal data, you can do so by contacting us at
          finelookstech@gmail.com . Please note that certain information may be
          retained for legal, accounting, or internal business purposes even
          after your account is deleted.
        </p>

        <h2 className="text-3xl sm:text-2xl md:text-3xl  my-3 ">
          7. Your Rights and Choices
        </h2>

        <p>
          {" "}
          You have certain rights concerning your personal data, which include:
        </p>

        <ul className="list-disc ml-6">
          <li>
            <strong>Access: </strong>You can request access to the personal data
            we have collected about you.
          </li>

          <li>
            <strong>Correction:</strong> You can request correction of
            inaccurate or incomplete data.
          </li>

          <li>
            <strong>Deletion:</strong> You can request the deletion of your
            personal data under certain circumstances.
          </li>

          <li>
            <strong>Portability:</strong> You may request a copy of your
            personal data in a structured, commonly used, and machine-readable
            format.
          </li>

          <li>
            <strong>Marketing Opt-Outs:</strong> You have the right to opt out
            of receiving marketing emails and promotions from us.
          </li>
        </ul>

        <p className="mt-2">
          To exercise any of your rights, please contact us at
          finelookstech@gmail.com. We will respond to your request in accordance
          with applicable data protection laws.
        </p>

        <h2 className="text-3xl sm:text-2xl md:text-3xl  my-3 ">
          8. Children’s Privacy
        </h2>

        <p>
          Fine Foods is not intended for use by children under the age of 13,
          and we do not knowingly collect personal information from children
          under this age. If we discover that we have inadvertently collected
          data from a child under 13, we will delete the information promptly.
          Parents or guardians who believe their child may have provided us with
          personal data should contact us immediately at
          finelookstech@gmail.com.
        </p>

        <h2 className="text-3xl sm:text-2xl md:text-3xl  my-3 ">
          9. International Data Transfers
        </h2>

        <p>
          Fine Foods operates globally, and as such, your personal data may be
          transferred and processed in countries outside of your residence,
          including countries that may not have the same level of data
          protection laws as your own. When such transfers occur, we take
          appropriate measures to protect your personal data in accordance with
          this Privacy Policy.
        </p>

        <h2 className="text-3xl sm:text-2xl md:text-3xl  my-3 ">
          10. Changes to This Privacy Policy
        </h2>

        <p>
          Fine Foods reserves the right to update or modify this Privacy Policy
          at any time to reflect changes in our practices or applicable laws. We
          will notify you of any significant changes by posting the updated
          Privacy Policy on this page and updating the effective date above.
          Your continued use of the app after such changes constitutes your
          acknowledgment and acceptance of the updated Privacy Policy.
        </p>

        <h2 className="text-3xl sm:text-2xl md:text-3xl  my-3 ">
          11. Contact Us
        </h2>

        <p className="mb-4">
          If you have any questions, concerns, or requests related to this
          Privacy Policy or your personal data, please contact us at:
        </p>

        <ul>
          <li>
            <strong>Email:</strong> finelookstech@gmail.com
          </li>
          <li>
            <strong>Phone:</strong> +966596482218
          </li>
        </ul>
      </div>
    </>
  );
};

export default PrivacyPol;
