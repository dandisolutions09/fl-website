import React from "react";
import Navbar from "../components/Navbar";

const CookieComponent = () => {
  return (
    // <div style={{ padding: "80px", fontFamily: "Arial, sans-serif" }}>

    <>
     <div className="sticky top-0 z-50 bg-[#00563B]">
          <Navbar />
        </div>

      <div className="p-[50px] md:p-[80px]">
        <h1 className="text-3xl sm:text-2xl md:text-3xl  my-3 ">About Us</h1>
        <p>
          At Fine Foods, we are passionate about food sustainability and making
          a positive impact on the environment. Our mission is simple: to save
          quality food from going to waste while offering consumers delicious
          meals at unbeatable prices. By connecting restaurants, cafes, and food
          businesses with customers, we provide a win-win solution that reduces
          waste and helps businesses generate revenue from surplus food.
        </p>
        <h2 className="text-3xl sm:text-2xl md:text-3xl  my-3 ">Our Story</h2>
        <p>
          In a world where millions of tons of perfectly good food are discarded
          every year, we knew there had to be a better way. Fine Foods was born
          out of the desire to tackle the growing issue of food waste while also
          addressing the rising cost of living. We saw an opportunity to create
          a platform that would help food businesses minimize waste and allow
          consumers to enjoy high-quality food at great value.
        </p>
        <p>
          What started as a simple idea has now blossomed into a
          community-driven movement to make food sustainability accessible to
          everyone.
        </p>
        <h2 className="text-3xl sm:text-2xl md:text-3xl  my-3 ">What We Do</h2>
        <p>
          Fine Foods offers an innovative platform where food businesses can
          sell surplus food that would otherwise go to waste. Through our app,
          restaurants, cafes, and stores create "Treat Bags" filled with fresh
          surplus food, available for consumers to purchase at a fraction of the
          original price. These bags contain a surprise selection of delicious
          food items, making it a fun and affordable way for users to discover
          new eateries and meals.
        </p>
        <p>
          Consumers benefit by getting high-quality food at low prices, while
          businesses benefit from reducing waste and turning excess stock into
          revenue.
        </p>
        <h2 className="text-3xl sm:text-2xl md:text-3xl  my-3 ">Our Mission</h2>
        <ul className="list-disc ml-6">
          <li>
            <strong>Save Food:</strong> We are dedicated to reducing food waste
            by ensuring that quality food finds its way to tables instead of
            landfills.
          </li>
          <li>
            <strong>Support Local Businesses:</strong> We empower local
            restaurants and food vendors by providing them with a platform to
            reach new customers and generate income from their surplus food.
          </li>
        </ul>
        <p className="mt-4">
          At Fine Foods, we believe that food is too valuable to be wasted, and
          by working together, we can build a more sustainable future.
        </p>
        <h2 className="text-3xl sm:text-2xl md:text-3xl  my-3 ">
          How it Works
        </h2>
        <ol className="list-decimal ml-6">
          <li>
            <strong>Discover Treat Bags:</strong> Browse our app to find nearby
            food businesses offering surplus Treat Bags.
          </li>
          <li>
            <strong>Purchase at a Discount:</strong> Buy your chosen Treat Bag
            at a discounted price, and enjoy high-quality food at incredible
            value.
          </li>

          <li>
            <strong>Pick Up and Enjoy:</strong> Collect your Treat Bag from the
            selected location within the specified time frame, then enjoy your
            meal knowing you’ve helped reduce food waste.
          </li>
        </ol>
        <h2 className="text-3xl sm:text-2xl md:text-3xl  my-3 ">
          Join Us in Our Mission
        </h2>
        <p>
          We invite you to join us on our journey to combat food waste and make
          sustainability a part of everyday life. By choosing Fine Foods, you’re
          not only enjoying great food—you’re making a difference. Whether
          you’re a food business looking to reduce waste or a consumer searching
          for delicious meals at low prices, we’re here to help you make a
          positive impact.
        </p>
        <p className="m-4 border-b border-gray-500 pb-4"></p>
        <h2 className="text-3xl sm:text-2xl md:text-3xl  my-3 ">
          Start saving food and saving money today!
        </h2>
        <p>
          Download the Fine Foods app and become part of a growing movement
          that’s changing the way we think about food waste.
        </p>
        <p className="m-4 border-b border-gray-500 pb-4"></p>
        <h2 className="text-3xl sm:text-2xl md:text-3xl  my-3 ">Our Values</h2>
        <ul className="list-disc ml-6">
          <li>
            <strong>Sustainability:</strong> We are driven by the goal of
            creating a more sustainable food system.
          </li>
          <li>
            <strong>Community:</strong> We support local businesses and bring
            consumers together with their favorite local eateries.
          </li>

          <li>
            <strong>Affordability:</strong> We believe everyone should have
            access to quality food at an affordable price.
          </li>

          <li>
            <strong>Innovation:</strong> We strive to innovate in the food
            space, offering new ways for businesses and consumers to connect.
          </li>
        </ul>
        <p className="m-4 border-b border-gray-500 pb-4"></p>
        <h2 className="text-3xl sm:text-2xl md:text-3xl  my-3 ">
          Get in Touch
        </h2>
        <p className="mb-4">
          If you have any questions, feedback, or want to partner with us, feel
          free to reach out:
        </p>

        <ul>
          <li>
            <strong>Email:</strong> finelookstech@gmail.com
          </li>
          <li>
            <strong>Phone:</strong> +966596482218
          </li>
        </ul>
        <p className="mt-4">
          Join the Fine Foods community and help us revolutionize the way we
          think about food!
        </p>
      </div>
    </>
  );
};

export default CookieComponent;
