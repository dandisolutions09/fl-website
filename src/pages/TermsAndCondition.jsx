import React from "react";
import Navbar from "../components/Navbar";

const TermsAndCondition = () => {
  return (
    <>
      <div className="sticky top-0 z-50 bg-[#00563B]">
          <Navbar />
        </div>

      <div className="p-[50px] md:p-[80px]">
        {/* <h1 className="text-3xl sm:text-2xl md:text-3xl  my-3 item-center">
        About Us
      </h1> */}

        <h1 className="text-5xl  my-3 text-center mb-4">
          Terms and Conditions
        </h1>

        <h1 className="font-bold my-4">Effective Date: 7/10/2024</h1>

        <p>
          Welcome to Fine Foods! These Terms and Conditions (hereinafter
          referred to as "Terms") govern your access and use of the Fine Foods
          app, website, and any related services (collectively, the “Services”).
          By using the Services, you agree to comply with and be bound by these
          Terms. If you do not agree to these Terms, you may not access or use
          the Services.
        </p>
        {/* <h2 className="text-3xl sm:text-2xl md:text-3xl  my-3 ">Our Story</h2> */}

        <p>
          Please read these Terms carefully before using the Fine Foods app.
        </p>

        <h2 className="text-3xl sm:text-2xl md:text-3xl  my-3 ">
          1. Acceptance of Terms
        </h2>

        <p>
          By registering for an account, using the Fine Foods app, or otherwise
          accessing our Services, you acknowledge that you have read,
          understood, and agree to be bound by these Terms, as well as our
          Privacy Policy, which is incorporated by reference.
        </p>

        <h2 className="text-3xl sm:text-2xl md:text-3xl  my-3 ">
          2. About Fine Foods
        </h2>

        <p>
          Fine Foods provides a platform that connects restaurants, stores, and
          food businesses with consumers to purchase surplus food, known as
          “Treat Bags,” at a discounted price. Our mission is to reduce food
          waste and offer high-quality food at low prices. Fine Foods acts as an
          intermediary, facilitating transactions between participating
          businesses and consumers.
        </p>
        {/* <ul className="list-disc ml-6">
        <li>
          <strong>Save Food:</strong> We are dedicated to reducing food waste by
          ensuring that quality food finds its way to tables instead of
          landfills.
        </li>
        <li>
          <strong>Support Local Businesses:</strong> We empower local
          restaurants and food vendors by providing them with a platform to
          reach new customers and generate income from their surplus food.
        </li>
      </ul> */}
        {/* <p className="mt-4">
        At Fine Foods, we believe that food is too valuable to be wasted, and by
        working together, we can build a more sustainable future.
      </p> */}
        <h2 className="text-3xl sm:text-2xl md:text-3xl  my-3 ">
          3. User Account and Responsibilities
        </h2>

        <h2 className="text-3xl sm:text-2xl md:text-2xl  my-3 ">
          3.1. Account Registration:
        </h2>

        <ul className="list-disc ml-6">
          <li>
            To use the Services, you must create an account by providing
            accurate and complete information, including your name, email
            address, and payment details. You are responsible for maintaining
            the confidentiality of your account credentials and for all
            activities that occur under your account.
          </li>
        </ul>

        <h2 className="text-3xl sm:text-2xl md:text-2xl  my-3 ">
          3.2. Account Security:
        </h2>

        <ul className="list-disc ml-6">
          <li>
            You agree to notify Fine Foods immediately of any unauthorized use
            of your account or any other breach of security. Fine Foods will not
            be liable for any loss or damage arising from unauthorized access to
            your account.
          </li>
        </ul>

        <h2 className="text-3xl sm:text-2xl md:text-2xl  my-3 ">
          3.3. User Eligibility:
        </h2>

        <ul className="list-disc ml-6">
          <li>
            You must be at least 18 years old to create an account and use the
            Services. By using the Services, you represent and warrant that you
            meet this age requirement.
          </li>
        </ul>

        <h2 className="text-3xl sm:text-2xl md:text-3xl  my-3 ">
          4. Use of Services
        </h2>

        <h2 className="text-3xl sm:text-2xl md:text-2xl  my-3 ">
          4.1. Permitted Use:
        </h2>

        <ul className="list-disc ml-6">
          <li>
            You agree to use the Services only for lawful purposes and in
            compliance with these Terms. You may not use the Services to engage
            in any unlawful activity or to violate any third-party rights.
          </li>
        </ul>

        <h2 className="text-3xl sm:text-2xl md:text-2xl  my-3 ">
          4.2. Prohibited Activities:
        </h2>

        <p>You are prohibited from:</p>

        <ul className="list-disc ml-6">
          <li>Using the Services for any fraudulent or illegal activities.</li>
          <li>
            Engaging in any conduct that restricts or inhibits anyone's use or
            enjoyment of the Services.
          </li>
          <li>
            Accessing or attempting to access any non-public areas of the
            Services without authorization.
          </li>
          <li>
            Reverse-engineering or attempting to extract the source code of the
            Services.
          </li>
          <li>Uploading viruses, malware, or other harmful components.</li>
        </ul>

        <h2 className="text-3xl sm:text-2xl md:text-3xl  my-3 ">
          5. Purchasing Treat Bags
        </h2>

        <h2 className="text-3xl sm:text-2xl md:text-2xl  my-3 ">
          5.1. How It Works:
        </h2>

        <ul className="list-disc ml-6">
          <li>
            Fine Foods allows participating restaurants and businesses to create
            Treat Bags consisting of surplus food. The content of the Treat Bags
            is determined by the business based on surplus availability and is
            sold to users at a discounted price. The exact contents of the Treat
            Bags are not disclosed to users prior to purchase.
          </li>
        </ul>

        <h2 className="text-3xl sm:text-2xl md:text-2xl  my-3 ">
          5.2. Order Process:
        </h2>

        <ul className="list-disc ml-6">
          <li>
            Users can browse, select, and purchase available Treat Bags from
            participating businesses through the Fine Foods app. Once a purchase
            is completed, users will receive a confirmation along with details
            for order pickup. Treat Bags must be picked up within the specified
            time window provided by the business.
          </li>
        </ul>

        <h2 className="text-3xl sm:text-2xl md:text-2xl  my-3 ">
          5.3. No Guarantees on Food Types or Preferences:
        </h2>

        <ul className="list-disc ml-6">
          <li>
            Since Treat Bags consist of surplus food, Fine Foods and the
            participating business make no guarantees regarding specific food
            items, dietary preferences, or restrictions (e.g., allergies,
            vegan/vegetarian). It is the user’s responsibility to consider these
            factors when purchasing a Treat Bag.
          </li>
        </ul>

        <h2 className="text-3xl sm:text-2xl md:text-3xl  my-3 ">
          6. Pricing and Payments
        </h2>

        <h2 className="text-3xl sm:text-2xl md:text-2xl  my-3 ">
          6.1. Pricing:
        </h2>

        <ul className="list-disc ml-6">
          <li>
            The prices for Treat Bags are set by the participating business.
            Fine Foods does not control the pricing of these items. Prices are
            displayed in the app before the user completes the purchase.
          </li>
        </ul>

        <h2 className="text-3xl sm:text-2xl md:text-2xl  my-3 ">
          6.2. Order Process:
        </h2>

        <ul className="list-disc ml-6">
          <li>
            Payments are processed securely through third-party payment
            processors. By making a purchase, you authorize Fine Foods and its
            payment processors to charge the payment method provided. Fine Foods
            does not store sensitive payment information, such as credit card
            details.
          </li>
        </ul>

        <h2 className="text-3xl sm:text-2xl md:text-2xl  my-3 ">
          6.3. Refunds:
        </h2>

        <ul className="list-disc ml-6">
          <li>
            All sales of Treat Bags are final. Fine Foods does not offer refunds
            or exchanges, except in cases where the purchased Treat Bag was
            unavailable for pickup due to the business’s error. In such cases,
            the user may contact Fine Foods’ customer support at  finelookstech@gmail.com for assistance.
          </li>
        </ul>

        <h2 className="text-3xl sm:text-2xl md:text-3xl  my-3 ">
          7. User Conduct and Responsibilities
        </h2>

        <p>By using the Services, you agree to adhere to the following:</p>

        <h2 className="text-3xl sm:text-2xl md:text-2xl  my-3 ">
          7.1. Respectful Behavior:
        </h2>

        <ul className="list-disc ml-6">
          <li>
            You must interact respectfully and courteously with participating
            businesses, other users, and Fine Foods staff. Any inappropriate or
            abusive behavior may result in suspension or termination of your
            account.
          </li>
        </ul>

        <h2 className="text-3xl sm:text-2xl md:text-2xl  my-3 ">
          7.2. Compliance with Local Laws:
        </h2>

        <ul className="list-disc ml-6">
          <li>
            You are responsible for complying with all applicable laws and
            regulations when using the Services, including food safety and
            consumer protection laws.
          </li>
        </ul>

        <h2 className="text-3xl sm:text-2xl md:text-2xl  my-3 ">
          7.3. Pickup Responsibility:
        </h2>

        <ul className="list-disc ml-6">
          <li>
            It is your responsibility to pick up your purchased Treat Bag within
            the designated time frame specified by the business. Fine Foods and
            participating businesses are not responsible for items that are not
            collected on time.
          </li>
        </ul>

        <h2 className="text-3xl sm:text-2xl md:text-3xl  my-3 ">
          8. Content Ownership and Licenses
        </h2>

        <h2 className="text-3xl sm:text-2xl md:text-2xl  my-3 ">
          8.1. Fine Foods Content:
        </h2>

        <ul className="list-disc ml-6">
          <li>
            All content provided by Fine Foods, including but not limited to
            logos, trademarks, text, images, and software, is owned by or
            licensed to Fine Foods and is protected by intellectual property
            laws. You are granted a limited, non-exclusive license to access and
            use the Services for personal, non-commercial purposes.
          </li>
        </ul>

        <h2 className="text-3xl sm:text-2xl md:text-2xl  my-3 ">
          8.2. User Content:
        </h2>

        <ul className="list-disc ml-6">
          <li>
            By submitting any content (e.g., reviews, comments) to Fine Foods,
            you grant us a worldwide, non-exclusive, royalty-free license to
            use, reproduce, modify, and distribute such content for the purpose
            of operating and improving the Services.
          </li>
        </ul>

        <h2 className="text-3xl sm:text-2xl md:text-3xl  my-3 ">
          9. Termination
        </h2>
        <p>
          Fine Foods reserves the right to suspend or terminate your account and
          access to the Services at any time for any reason, including, but not
          limited to:
        </p>

        <ul className="list-disc ml-6">
          <li>Violation of these Terms.</li>
          <li>Fraudulent activity.</li>
          <li>Failure to comply with any applicable laws or regulations.</li>
        </ul>

        <p className="mt-4">
          Upon termination, your right to use the Services will immediately
          cease, and any outstanding balances owed to Fine Foods or third-party
          payment processors must be settled.
        </p>

        <h2 className="text-3xl sm:text-2xl md:text-3xl  mt-8 ">
          10. Content Ownership and Licenses
        </h2>

        <h2 className="text-3xl sm:text-2xl md:text-2xl  my-3 ">
          10.1. No Warranty:
        </h2>

        <ul className="list-disc ml-6">
          <li>
            Fine Foods provides the Services on an “as is” and “as available”
            basis without warranties of any kind, either express or implied,
            including but not limited to warranties of merchantability, fitness
            for a particular purpose, or non-infringement. Fine Foods does not
            guarantee the accuracy, completeness, or availability of the
            Services.
          </li>
        </ul>

        <h2 className="text-3xl sm:text-2xl md:text-2xl  my-3 ">
          10.2. Limitation of Liability:
        </h2>

        <ul className="list-disc ml-6">
          <li>
            To the maximum extent permitted by law, Fine Foods and its
            affiliates, partners, and employees shall not be liable for any
            direct, indirect, incidental, consequential, or punitive damages
            arising out of or related to your use of the Services, including but
            not limited to any errors or omissions in content, loss of data,
            loss of profit, or business interruption.
          </li>
        </ul>

        <h2 className="text-3xl sm:text-2xl md:text-3xl  my-3 ">
          11. Indemnification
        </h2>
        <p>
          You agree to indemnify, defend, and hold harmless Fine Foods and its
          affiliates from any claims, liabilities, damages, losses, or expenses
          (including reasonable attorney’s fees) arising out of or related to:
        </p>

        <ul className="list-disc ml-6">
          <li>Your use of the Services.</li>
          <li>Your violation of these Terms.</li>
          <li>
            Your violation of any third-party rights, including intellectual
            property rights.
          </li>
        </ul>

        <h2 className="text-3xl sm:text-2xl md:text-3xl  my-3 ">
          12. Modifications to Terms
        </h2>

        <p>
          Fine Foods reserves the right to modify or update these Terms at any
          time. We will notify you of any material changes by posting the
          updated Terms on the app or website and updating the effective date at
          the top. Your continued use of the Services after any changes
          constitutes your acceptance of the new Terms.
        </p>

        <h2 className="text-3xl sm:text-2xl md:text-3xl  my-3 ">
          13. Governing Law and Dispute Resolution
        </h2>

        <p>
          These Terms are governed by and construed in accordance with the laws
          of Saudi Arabia, without regard to its conflict of law
          provisions. Any dispute arising out of or relating to these Terms or
          the Services will be resolved through binding arbitration in
          accordance with the rules of the Saudi Arabia, and
          judgment on the arbitration award may be entered in any court having
          jurisdiction.
        </p>

        <h2 className="text-3xl sm:text-2xl md:text-3xl  mt-8 ">
          14. Miscellaneous
        </h2>

        <h2 className="text-3xl sm:text-2xl md:text-2xl  my-3 ">
          14.1. Entire Agreement:
        </h2>

        <ul className="list-disc ml-6">
          <li>
            These Terms, along with our Privacy Policy, constitute the entire
            agreement between you and Fine Foods regarding the use of the
            Services and supersede any prior agreements.
          </li>
        </ul>

        <h2 className="text-3xl sm:text-2xl md:text-2xl  my-3 ">
          14.2. Severability:
        </h2>

        <ul className="list-disc ml-6">
          <li>
            If any provision of these Terms is found to be invalid or
            unenforceable, the remaining provisions will continue in full force
            and effect.
          </li>
        </ul>

        <h2 className="text-3xl sm:text-2xl md:text-2xl  my-3 ">
          14.3. No Waiver:
        </h2>

        <ul className="list-disc ml-6">
          <li>
            If any provision of these Terms is found to be invalid or
            unenforceable, the remaining provisions will continue in full force
            and effect.
          </li>
        </ul>

        <h2 className="text-3xl sm:text-2xl md:text-3xl  my-3 ">
          15. Contact Us
        </h2>

        <p>
          If you have any questions, comments, or concerns regarding these
          Terms, please contact us at:
        </p>

        <strong> Fine Foods</strong>
        <p>finelookstech@gmail.com</p>
      </div>
    </>
  );
};

export default TermsAndCondition;
