import React from "react";

export default function PrivacyPolicy1() {
  return (
    <>
      <div className="h-auto">
        <div className="flex flex-col justify-center items-center place-content-center mt-[100px] mb-4">
          <h1 className="text-5xl underline my-3">Privacy Policy</h1>
          <h1 className="font-bold my-4">
            Privacy and Cookie Policy - Last updated May 2024
          </h1>
          <div className="px-[400px] text-left">
            <p>
              This Privacy Policy describes Our policies and procedures on the
              collection, use, and disclosure of Your information when You use
              the Service and tells You about Your privacy rights and how the
              law protects You. We use Your Personal data to provide and improve
              the Service. By using the Service, You agree to the collection and
              use of information in accordance with this Privacy Policy. This
              Privacy Policy is maintained by FineLooks Technologies.
            </p>
            <p>
              This privacy and cookies policy ("Privacy Policy") details data
              used on www.Fine Foodsapp.com (the "Websites"), the FineLooks
              Customer mobile application software and the FineLooks Partner
              application software and website at Fine Foodsapp.com, both of which
              are available for download from App Store and Google Play (the
              "Apps"), customizable websites hosted on Fine Foodsapp.com, through
              which customers can make bookings directly with Partners at
              Fine Foodsapp.com. Fine Looks Technologies Limited is the data
              controller for your information - registered address: Kemp House
              160 City Road, London EC1V 2NX ("FineLooks", "we", "our" or "us").
              Contact us at support@Fine Foodsapp.com.
            </p>
            <p>
              We are committed to protecting the privacy of our users and
              customers. This Privacy Policy is intended to inform you how we
              gather, define, and use information that could identify you, such
              as your name, email address, address, other contact details or
              online identifiers, other information that you provide to us when
              using the Platform ("Personal Information") and also what Cookies
              we use. Please take a moment to read this Privacy Policy
              carefully.
            </p>
            <h2>1. Information We Collect</h2>
            <p>We may collect the following information:</p>
            <ul>
              <li>
                Personal identification information (Name, email address, phone
                number, etc.)
              </li>
              <li>
                Usage Data and Cookies to monitor and analyze the use of our
                Service, to contact You, and for other purposes detailed in this
                policy.
              </li>
              <li>
                Information from Third-Party Social Media Services, such as your
                name, profile picture, and other personal details when you
                connect to our service through these platforms.
              </li>
            </ul>
            <h2>2. How We Use Your Information</h2>
            <p>
              We may use the information we collect for various purposes,
              including:
            </p>
            <ul>
              <li>
                To provide and maintain our Service, including monitoring the
                usage of our Service.
              </li>
              <li>To manage your account and provide customer support.</li>
              <li>
                To contact you with newsletters, marketing or promotional
                materials, and other information that may be of interest to you.
              </li>
              <li>
                To provide you with targeted advertisements and marketing
                communications.
              </li>
              <li>
                To comply with legal obligations and protect our legal rights.
              </li>
            </ul>
            <h2>3. Sharing Your Personal Information</h2>
            <p>
              We may share Your personal information in the following
              situations:
            </p>
            <ul>
              <li>
                With Service Providers: To monitor and analyze the use of our
                Service, to contact You.
              </li>
              <li>
                With Affiliates: For purposes consistent with this Privacy
                Policy.
              </li>
              <li>
                With Business Partners: To offer You certain products, services,
                or promotions.
              </li>
              <li>
                With other users: When You share personal information in public
                areas, such information may be viewed by all users and may be
                publicly distributed outside.
              </li>
              <li>
                With Third-Party Social Media Services: When You interact with
                other users through these services.
              </li>
              <li>
                With law enforcement and regulatory authorities: To comply with
                legal obligations or to protect our rights.
              </li>
            </ul>
            <h2>4. Cookies</h2>
            <p>
              Our Platform uses Cookies and similar technologies to enhance your
              user experience. The types of Cookies we use include:
            </p>
            <ul>
              <li>
                <strong>Strictly Necessary Cookies:</strong> These cookies are
                necessary for the website to function and cannot be switched off
                in our systems.
              </li>
              <li>
                <strong>Performance Cookies:</strong> These cookies allow us to
                count visits and traffic sources to measure and improve the
                performance of our site.
              </li>
              <li>
                <strong>Functional Cookies:</strong> These cookies enable the
                website to provide enhanced functionality and personalization.
              </li>
            </ul>
            <h2>5. Your Data Protection Rights</h2>
            <p>You have the following data protection rights:</p>
            <ul>
              <li>
                The right to access, update, or delete the information we have
                on you.
              </li>
              <li>
                The right to rectification if your information is inaccurate or
                incomplete.
              </li>
              <li>
                The right to object to our processing of your personal data.
              </li>
              <li>
                The right to request that we restrict the processing of your
                personal data.
              </li>
              <li>The right to data portability.</li>
              <li>
                The right to withdraw consent at any time where we rely on your
                consent to process your personal information.
              </li>
            </ul>
            <p>
              To exercise these rights, please contact us at
              support@Fine Foodsapp.com.
            </p>
            <h2>6. Retention of Your Personal Data</h2>
            <p>
              We retain your Personal Information for as long as necessary to
              fulfill the purposes outlined in this Privacy Policy or as
              required by law.
            </p>
            <h2>7. Changes to This Privacy Policy</h2>
            <p>
              We may update our Privacy Policy from time to time. We will notify
              you of any changes by posting the new Privacy Policy on this page.
            </p>
            <h2>8. Contact Us</h2>
            <p>
              If you have any questions about this Privacy Policy, you can
              contact us at support@Fine Foodsapp.com.
            </p>
          </div>
        </div>
      </div>
    </>
  );
}
