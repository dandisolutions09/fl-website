import React from "react";

export default function Usersguide() {
  return (
    <>
      <div className="h-auto">
        <div className="flex flex-col justify-center items-center place-content-center mt-[100px]">
          <h1 className="text-5xl underline my-3">
            User Generated Content Policy
          </h1>
          <h1>User Generated Content Policy</h1>
          <h1 className="font-bold my-4"> Last updated May 2024</h1>
          <h1 className="px-[400px]">
            Please read this User Generated Content Policy carefully before you
            submit UGC to our Website or via our Apps, as this policy and our
            Website & App Terms of Use will apply to your use of our Website,
            our Apps and the UGC you submit to it or them. We recommend that you
            print a copy of this for future reference. By using our Website or
            our Apps and submitting UGC to our Website or via our Apps, you
            confirm that you accept this User Generated Content Policy, our
            Privacy and Cookie Policy and our Website & App Terms of Use and
            that you agree to comply with them. If you do not agree to these
            terms and policies, you must not use our Website or Apps or submit
            UGC to (or via) it (or them). Capitalised but undefined words in
            this User Generated Content Policy shall have the meanings ascribed
            to them in our Website & App Terms of Use.Your UGCAll content
            submitted to our Website or via our Apps by you (or on your behalf)
            via your User account (or other social media account, if
            applicable), including without limitation, your name, biographical
            information and all other names, usernames, pseudonyms, text,
            likenesses, graphics, logos, marks, images, photographs, code, and
            all other information and material shall be called your "UGC" for
            short.You agree to submit UGC to the Website and via our Apps in
            accordance with the following rules (in particular, the Legal
            Standards and the Review Guidelines, as those terms are defined
            below). Please use caution and common sense when submitting UGC to
            the Website or via our App.Publication of your UGC will be at our
            sole discretion and we are entitled to make additions or deletions
            to your UGC prior to publication, after publication or to refuse
            publication.Please note, any UGC you submit to our Website or via
            our Apps will be considered non-confidential and
            non-proprietary.Rights, permissions & waiversYou hereby grant to
            Fine Looks Technologies Limited (t/a FineLooks) and any of our group
            companies and affiliates a non-exclusive, perpetual, irrevocable,
            transferable, royalty-free licence (including full rights to
            sub-license) to use, reproduce and publish your UGC (including,
            without limitation, the right to adapt, alter, amend or change your
            UGC) in any media or format (whether known now or invented in the
            future) throughout the world without restriction.You warrant,
            represent and undertake to us that all UGC you submit is your own
            work or that you have obtained all necessary rights and permissions
            of the relevant owner of the work and that you have all relevant
            rights in your UGC to enable you to grant the rights and permissions
            in this clause 2.Where your UGC contains images of people or names
            or identifies individuals, you warrant, represent and undertake to
            us as follows:that all featured or identified individuals that are
            over the age of 18 and have expressly consented to their appearance
            in the UGC and to you submitting the UGC to our Website or via our
            Apps, andwhere featured or identified individuals are under the age
            of 18, that you either:are the parent or legal guardian or such
            featured or identified individuals, orhave obtained the express
            consent from a parent or legal guardian of such featured or
            identified individuals to their appearance in the UGC and to you
            submitting the UGC to our Website or via our Apps.You hereby
            unconditionally and irrevocably waive and agree not to assert (or
            procure the same from any third party where applicable) any and all
            moral rights and any other similar rights and all right of publicity
            and privacy in any country in the world in connection with your UGC,
            to the maximum extent permissible by law.Content standards – legal
            standardsYou warrant, represent and undertake to us that your UGC
            (including its use, publication and/or exploitation by us) shall
            not:infringe the copyrights or database rights, trademarks, rights
            of privacy, publicity or other intellectual property or other rights
            of any other person or entity; and/orcontain any material which is
            defamatory of any person; and/orcontain misleading or deceptive
            statements or omissions or misrepresentation as to your identity
            (for example, by impersonating another person) or your affiliation
            with any person or entity; and/orbreach any legal or fiduciary duty
            owed to a third party, such as a contractual duty or a duty of
            confidence; and/oradvocate, promote, or assist discrimination based
            on race, sex, religion, nationality, disability, sexual orientation
            or age; and/orcontain any malicious code, such as viruses, worms,
            Trojan horses or other potentially harmful programmes, codes or
            material; and/orviolate any other applicable law, statute,
            ordinance, rule or regulation, (together, or individually the "Legal
            Standards").If your UGC contains any material that is not owned by
            or licensed to you and/or which is subject to third party rights,
            you are responsible for obtaining, prior to submission of your UGC,
            all releases, consents and/or licenses necessary to permit use and
            exploitation of your UGC by us without additional compensation.
            Please see clause 2 above for further details.Content standards –
            review guidelinesYou warrant, represent and undertake to us that
            your UGC:is accurate, where it states facts; and/oris genuinely
            held, where it states opinions (for example, in product or services
            reviews).You further warrant, represent and undertake to us that
            your UGC (including its use, publication and/or exploitation by us)
            shall be compliant with our Review Guidelines, which can be found
            here: https://Fine Foodsapp.com/help-center Any UCG which is in breach
            of our Review Guidelines or is otherwise:is obscene, hateful,
            inflammatory, offensive or in any other way falls below commonly
            accepted standards of taste and decency in the UK; and/oris
            reasonably likely to harass, upset, embarrass or alarm a person
            (including, by way of example only, so called "trolling" or
            cyber-bullying); and/oris threatening, abusive or invades another's
            privacy, or causes annoyance, inconvenience or anxiety; and/oris
            sexually explicit; and/oradvocates, promotes, assists or depicts
            violence; and/oradvocates promote or assists any illegal activity or
            unlawful act or omission; and/orcould be deemed to be unsolicited or
            unauthorised advertising, promotional material, junk mail, or spam
            (including without limitation chain letters, pyramid schemes or
            other forms of solicitation or advertisements, commercial or
            otherwise); and/orgives the impression that it emanates from Fine
            Looks or is endorsed or connected with us, if this is not the case,
            will be removed from the Site.Consequences of breachWe will
            determine, in our discretion, whether you have failed to comply with
            this UGC Policy when submitting UGC to our Website or via our Apps.
            If you have failed to comply, we reserve the right in our sole
            discretion to suspend you from using the Website and/or our Apps
            without notice to you and/or to edit or remove (in whole or part)
            any of your UGC from our Website and our Apps on a temporary or
            permanent basis.Notwithstanding clause 5.1 above, if you or your UGC
            does not comply with this UGC Policy, and as a result of this, we
            suffer any loss or damage, you will be liable to us and hereby agree
            to indemnify us for any such loss or damage. This means that you
            will be responsible for any loss or damage we suffer as a result of
            your failure to comply with this UGC Policy, including but not
            limited to our Legal Standards and/or Review Guidelines.We also
            reserve the right:to pass on any UGC that gives us concern to the
            relevant authorities; andto disclose your identity to any third
            party (or their professional advisor) who claims that any of your
            UGC constitutes a violation of their intellectual property rights,
            or of their right to privacy.Changes to this UGC PolicyWe may change
            this UGC Policy from time to time, in which case an up to date
            version will be available via the Website and our Apps. You should
            check this UGC Policy regularly to ensure that you are happy with
            any changes. You will be deemed to have accepted any changes to this
            UGC Policy after you have been notified of the changes on our
            Website or our Apps and/ or if you continue to access or use the
            Website or our Apps, where the updated UGC Policy will be available
            for you to view.
          </h1>
        </div>
      </div>
    </>
  );
}
