

import React from "react";
import Navbar from "../components/Navbar";
import mainImage from "../assets/imagesforwebsite1/phone_mockups/file.png";
import { motion } from "framer-motion";

const AboutPage = () => {
  return (
    <>
        <div className="sticky top-0 z-50 bg-[#00563B]">
          <Navbar />
        </div>

      <div className="p-[50px] md:p-[80px]">
        <h1 className="text-5xl my-3 text-center mb-4">About Us</h1>

        {/* Container for the image and text */}
        <div className="md:flex md:items-start">
          {/* Image with animation */}
          <div className="md:w-1/3 md:mr-8 flex justify-center mb-4 md:mb-0">
            <motion.img
              src={mainImage}
              alt="description"
              initial={{ x: 250, opacity: 0 }}
              animate={{ x: 0, opacity: 1 }}
              transition={{ duration: 1.0 }}
              className="w-full h-auto"
            />
          </div>

          {/* Text that wraps around the image */}
          <div className="md:w-2/3">
            <p>
              At Fine Foods, we are passionate about food sustainability and
              making a positive impact on the environment. Our mission is
              simple: to save quality food from going to waste while offering
              consumers delicious meals at unbeatable prices. By connecting
              restaurants, cafes, and food businesses with customers, we provide
              a win-win solution that reduces waste and helps businesses
              generate revenue from surplus food.
            </p>
            <p>
              Fine Foods offers an innovative platform where food businesses can
              sell surplus food that would otherwise go to waste. Through our
              app, restaurants, cafes, and stores create "Treat Bags" filled
              with fresh surplus food, available for consumers to purchase at a
              fraction of the original price. These bags contain a surprise
              selection of delicious food items, making it a fun and affordable
              way for users to discover new eateries and meals.
            </p>
          </div>
        </div>

        <h2 className="text-3xl sm:text-2xl md:text-3xl  my-3">Our Story</h2>
        <p>
          In a world where millions of tons of perfectly good food are discarded
          every year, we knew there had to be a better way. Fine Foods was born
          out of the desire to tackle the growing issue of food waste while also
          addressing the rising cost of living. We saw an opportunity to create
          a platform that would help food businesses minimize waste and allow
          consumers to enjoy high-quality food at great value.
        </p>
        <p>
          What started as a simple idea has now blossomed into a
          community-driven movement to make food sustainability accessible to
          everyone.
        </p>

        <h2 className="text-3xl sm:text-2xl md:text-3xl  my-3">What We Do</h2>
        <p>
          Consumers benefit by getting high-quality food at low prices, while
          businesses benefit from reducing waste and turning excess stock into
          revenue.
        </p>

        <h2 className="text-3xl sm:text-2xl md:text-3xl  my-3">Our Mission</h2>
        <ul className="list-disc ml-6">
          <li>
            <strong>Save Food:</strong> We are dedicated to reducing food waste
            by ensuring that quality food finds its way to tables instead of
            landfills.
          </li>
          <li>
            <strong>Support Local Businesses:</strong> We empower local
            restaurants and food vendors by providing them with a platform to
            reach new customers and generate income from their surplus food.
          </li>
        </ul>
        <p className="mt-4">
          At Fine Foods, we believe that food is too valuable to be wasted, and
          by working together, we can build a more sustainable future.
        </p>

        {/* Remaining Content */}
        <h2 className="text-3xl sm:text-2xl md:text-3xl  my-3">How it Works</h2>
        <ol className="list-decimal ml-6">
          <li>
            <strong>Discover Treat Bags:</strong> Browse our app to find nearby
            food businesses offering surplus Treat Bags.
          </li>
          <li>
            <strong>Purchase at a Discount:</strong> Buy your chosen Treat Bag
            at a discounted price, and enjoy high-quality food at incredible
            value.
          </li>
          <li>
            <strong>Pick Up and Enjoy:</strong> Collect your Treat Bag from the
            selected location within the specified time frame, then enjoy your
            meal knowing you’ve helped reduce food waste.
          </li>
        </ol>

        <h2 className="text-3xl sm:text-2xl md:text-3xl  my-3">
          Join Us in Our Mission
        </h2>
        <p>
          We invite you to join us on our journey to combat food waste and make
          sustainability a part of everyday life. By choosing Fine Foods, you’re
          not only enjoying great food—you’re making a difference. Whether
          you’re a food business looking to reduce waste or a consumer searching
          for delicious meals at low prices, we’re here to help you make a
          positive impact.
        </p>

        {/* Contact Section */}
        <h2 className="text-3xl sm:text-2xl md:text-3xl  my-3">Get in Touch</h2>
        <ul>
          <li>
            <strong>Email:</strong> finelookstech@gmail.com
          </li>
          <li>
            <strong>Phone:</strong> +966596482218
          </li>
        </ul>
      </div>
    </>
  );
};

export default AboutPage;
