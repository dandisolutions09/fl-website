// import React, { useState, useEffect } from "react";
// import { IoIosArrowBack, IoIosArrowForward } from "react-icons/io";
// // import { MdBrightness1 } from "react-icons/md";
// // import { MdGames } from "react-icons/md";
// // import { IoLogoGameControllerB } from "react-icons/io";

// const slideStyles = {
//   width: "100%",
//   height: "100%",
//   backgroundSize: "cover",
//   backgroundPosition: "center",
//   transition: "opacity 0.5s ease-in-out",
// };

// const rightArrowStyles = {
//   position: "absolute",
//   top: "50%",
//   transform: "translate(0, -50%)",
//   right: "32px",
//   color: "#fff",
//   zIndex: 1,
//   cursor: "pointer",
// };

// const leftArrowStyles = {
//   position: "absolute",
//   top: "50%",
//   transform: "translate(0, -50%)",
//   left: "32px",
//   color: "#fff",
//   zIndex: 1,
//   cursor: "pointer",
// };

// const sliderStyles = {
//   position: "relative",
//   height: "100%",
//   overflow: "hidden",
// };

// const dotsContainerStyles = {
//   display: "flex",
//   flexDirection: "row",
//   justifyContent: "center",
//   position: "absolute",
//   right: "50%",
//   bottom: "10%",
//   gap: "2rem",
// };

// const dotStyle = {
//   margin: "0 3px",
//   cursor: "pointer",
//   fontSize: "20px",
// };

// // const icons = [
// //   <MdBrightness1 size={24} />,
// //   <MdGames size={24} />,
// //   <IoLogoGameControllerB size={24} />,
// // ]; // Array of icons

// const ImageSlider = ({ slides }) => {
//   const [currentIndex, setCurrentIndex] = useState(0);
//   const [activeDot, setActiveDot] = useState(null);

//   console.log("Slides", slides);

//   // Function to go to the next slide
//   const goToNext = () => {
//     const isLastSlide = currentIndex === slides.length - 1;
//     const newIndex = isLastSlide ? 0 : currentIndex + 1;
//     setCurrentIndex(newIndex);
//   };

//   useEffect(() => {
//     const interval = setInterval(goToNext, 6000); // Slide every 1 second

//     return () => clearInterval(interval); // Cleanup function
//   }, [currentIndex]);

//   const goToSlide = (slideIndex) => {
//     setCurrentIndex(slideIndex);
//     setActiveDot(slideIndex);
//   };

//   const slideStylesWidthBackground = {
//     ...slideStyles,
//     backgroundImage: `url(${slides[currentIndex].url})`,
//   };

//   return (
//     <div style={sliderStyles}>
//       <div>
//         <div
//           onClick={() =>
//             setCurrentIndex(
//               currentIndex - 1 >= 0 ? currentIndex - 1 : slides.length - 1
//             )
//           }
//           style={leftArrowStyles}
//         >
//           <IoIosArrowBack size={24} />
//         </div>
//         <div
//           onClick={() =>
//             setCurrentIndex(
//               currentIndex + 1 < slides.length ? currentIndex + 1 : 0
//             )
//           }
//           style={rightArrowStyles}
//         >
//           <IoIosArrowForward size={24} />
//         </div>
//       </div>
//       <div
//         className="w-full h-full flex  "
//         style={{
//           transition: "transform 1s ease, opacity 0.5s ease-in-out",
//           transform: `translateX(${-100 * currentIndex}%)`,
//         }}
//       >
//         {slides.map((slides, index) => (
//           <img
//             key={index}
//             src={slides.url}
//             alt={`slides-${index}`} // Providing a meaningful alt text
//             className="object-cover w-full h-full block shrink-0 grow-0 "
//           />
//         ))}
//       </div>
//       <div className=" absolute left-1/2 sm:top-[20%] md:top-[43%] top-[20%] transform -translate-x-1/2 text-center flex flex-col sm:flex-row gap-0 sm:gap-2">
//         {/* {slides[currentIndex].buttons.map((button, index) => (
//           <button
//             className="bg-[#EE9B00] text-gray-800 p-3 sm:p-2 md:p-2 lg:p-3 rounded mt-6 font-semibold  sm:w-[210px] md:w-[150px]  w-[200px]   "
//             key={index}
//             onClick={button.onClick}
//           >
//             {button.label}
//           </button>
//         ))} */}
//       </div>
//       <div className="flex flex-row justify-center absolute sm:right-[43%] right-[32%] sm:top-[80%] top-[90%] bottom-10% gap-x-8">
//         {slides.map((slide, slideIndex) => (
//           <div
//             style={{
//               ...dotStyle,
//               color:
//                 activeDot === slideIndex
//                   ? "rgb(249 115 22)"
//                   : "rgb(226 232 240)",
//             }}
//             key={slideIndex}
//             onClick={() => goToSlide(slideIndex)}
//           >
//             {/* {currentIndex === slideIndex
//               ? icons[0]
//               : icons[(slideIndex + 1) % icons.length]} */}
//           </div>
//         ))}
//       </div>
//       <div
//         className={`absolute top-[85%] left-[${slides[currentIndex].leftPercentage}]  `}
//       >
//         {slides[currentIndex].title.map((title, index) => (
//           <div key={index} className="">
//             <h1 className="sm:text-4xl text-3xl  font-bold text-white">
//               {title.label1}
//             </h1>
//             <p className="sm:text-2xl text-xl font-semibold">{title.label2}</p>
//             <p className="sm:text-xl text-lg">{title.label3}</p>
//           </div>
//         ))}
//       </div>
//       <div className="blur hero-blur absolute"></div>
//       <div className="blur hero-blur absolute"></div>
//       <div className="blur hero-blur absolute"></div>
//       <div className="blur hero-blur absolute"></div>
//     </div>
//   );
// };

// export default ImageSlider;
import React, { useState, useEffect } from "react";
import { IoIosArrowBack, IoIosArrowForward } from "react-icons/io";

const slideStyles = {
  width: "100%",
  height: "100%",
  backgroundSize: "cover",
  backgroundPosition: "center",
  transition: "opacity 0.5s ease-in-out",
};

const rightArrowStyles = {
  position: "absolute",
  top: "50%",
  transform: "translate(0, -50%)",
  right: "32px",
  color: "#fff",
  zIndex: 1,
  cursor: "pointer",
};

const leftArrowStyles = {
  position: "absolute",
  top: "50%",
  transform: "translate(0, -50%)",
  left: "32px",
  color: "#fff",
  zIndex: 1,
  cursor: "pointer",
};

const sliderStyles = {
  position: "relative",
  height: "100%",
  overflow: "hidden",
};

const dotsContainerStyles = {
  display: "flex",
  flexDirection: "row",
  justifyContent: "center",
  position: "absolute",
  right: "50%",
  bottom: "10%",
  gap: "2rem",
};

const dotStyle = {
  margin: "0 3px",
  cursor: "pointer",
  fontSize: "20px",
};

const ImageSlider = ({ slides }) => {
  console.log("slides", slides);
  const [currentIndex, setCurrentIndex] = useState(0);
  const [activeDot, setActiveDot] = useState(null);

  const goToNext = () => {
    const isLastSlide = currentIndex === slides.length - 1;
    const newIndex = isLastSlide ? 0 : currentIndex + 1;
    setCurrentIndex(newIndex);
  };

  useEffect(() => {
    const interval = setInterval(goToNext, 6000);

    return () => clearInterval(interval);
  }, [currentIndex]);

  const goToSlide = (slideIndex) => {
    setCurrentIndex(slideIndex);
    setActiveDot(slideIndex);
  };

  const slideStylesWidthBackground = {
    ...slideStyles,
    backgroundImage: `url(${slides[currentIndex].url})`,
  };

  return (
    <div style={sliderStyles}>
      <div>
        <div
          onClick={() =>
            setCurrentIndex(
              currentIndex - 1 >= 0 ? currentIndex - 1 : slides.length - 1
            )
          }
          style={leftArrowStyles}
        >
          <IoIosArrowBack size={24} />
        </div>
        <div
          onClick={() =>
            setCurrentIndex(
              currentIndex + 1 < slides.length ? currentIndex + 1 : 0
            )
          }
          style={rightArrowStyles}
        >
          <IoIosArrowForward size={24} />
        </div>
      </div>
      <div
        className="w-full h-full flex"
        style={{
          transition: "transform 1s ease, opacity 0.5s ease-in-out",
          transform: `translateX(${-100 * currentIndex}%)`,
        }}
      >
        {slides.map((slides, index) => (
          <img
            key={index}
            src={slides.url}
            alt={`slides-${index}`}
            className="object-cover w-full h-full block shrink-0 grow-0"
          />
        ))}
      </div>
      {/* Gradient Hue at the Bottom */}
      <div
        style={{
          position: "absolute",
          bottom: 0,
          left: 0,
          width: "100%",
          height: "30%", // Adjust height as needed
          background:
            "linear-gradient(to top, rgba(0, 0, 0, 0.8), transparent)",
        }}
      ></div>
      {/* Title and Text Section */}
      <div
        className="absolute top-[85%]"
        style={{ left: slides[currentIndex].leftPercentage }}
      >
        {slides[currentIndex].title.map((title, index) => (
          <div key={index}>
            <h1 className="sm:text-4xl text-sm font-bold text-white">
              {title.label1}
            </h1>
            <p className="sm:text-2xl text-xl font-semibold">{title.label2}</p>
            <p className="sm:text-xl text-lg">{title.label3}</p>
          </div>
        ))}
      </div>
      <div className="flex flex-row justify-center absolute sm:right-[43%] right-[32%] sm:top-[80%] top-[90%] bottom-10% gap-x-8">
        {slides.map((slide, slideIndex) => (
          <div
            style={{
              ...dotStyle,
              color:
                activeDot === slideIndex
                  ? "rgb(249 115 22)"
                  : "rgb(226 232 240)",
            }}
            key={slideIndex}
            onClick={() => goToSlide(slideIndex)}
          ></div>
        ))}
      </div>
    </div>
  );
};

export default ImageSlider;
