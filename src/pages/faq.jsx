import React from "react";
import Navbar from "../components/Navbar";

import mainImage from "../assets/imagesforwebsite1/phone_mockups/2_Mockup.png";

const Faq = () => {
  return (
    // <div style={{ padding: "80px", fontFamily: "Arial, sans-serif" }}>

    <>
      <div className="sticky top-0 z-50 bg-[#00563B]">
          <Navbar />
        </div>
      <div className="p-[50px] md:p-[80px]">
        {/* Hero Image */}
        <div className="mt-8 md:mt-0 md:w-1/2 flex justify-center h-[400px] w-[300px]">
          <img
            src={mainImage}
            alt="Smiling woman holding toys"

            // className="max-w-xs md:max-w-md"
          />

          {/* Image Animation from Right */}
          {/* <motion.img
            src={mainImage}
            alt="description"
            initial={{ x: 250, opacity: 0 }}
            animate={{ x: 0, opacity: 1 }}
            transition={{ duration: 1.0 }}
          /> */}
        </div>
        <h1 className="text-5xl  my-3 text-center mb-4">
          Frequently Asked Questions
        </h1>
        <h2 className="text-3xl font-bold sm:text-2xl md:text-3xl  my-3 ">
          Why Partner up with Fine Foods?
        </h2>
        <p>
          Nearly a third of the world&#39;s food goes to waste, which is a huge
          loss for our planet. At Fine Foods, we&#39;re changing that. By
          partnering with us to sell your surplus food to food reduce waste,
          attract new customers, and increase your revenue. Our easy-to-use
          platform ensures your unsold food finds a happy home, contributing to
          a sustainable future.
        </p>
        <p>
          What started as a simple idea has now blossomed into a
          community-driven movement to make food sustainability accessible to
          everyone.
        </p>
        <h2 className="text-3xl font-bold sm:text-2xl md:text-3xl  my-3 ">
          What are Fine Foods Treat Bags?
        </h2>{" "}
        <p>
          Fine Foods Treat Bags are your delicious surplus foods that can then
          be offered to consumers at a lower cost, making it easier to enjoy
          great meals while reducing food waste. Customers nearby can pick up
          the Treat Bags via the Fine Foods app at a time that works for both of
          you. It&#39;s a great way to attract new customers, grow your
          business, and help fight food waste.
        </p>
        <h2 className="text-3xl font-bold sm:text-2xl md:text-3xl  my-3 ">
          What are the costs to join the Fine Foods platform and how much will I
          earn per Surprise Bag sold?
        </h2>{" "}
        <p>
          There are no joining fees. Fine Foods takes a 25% fee for each Treat
          Bag sold on our platform, and you keep the rest. You can see all the
          details about your Treat Bags on the app.
        </p>
        <h2 className="text-3xl font-bold sm:text-2xl md:text-3xl  my-3 ">
          How do I receive my earnings?
        </h2>{" "}
        <p>
          At the end of each month, your earnings will be transferred to your
          bank account, with our fees deducted before the payout. You will need
          to add your bank account information so we can easily send you your
          payouts.
        </p>
        <h2 className="text-3xl font-bold sm:text-2xl md:text-3xl  my-3 ">
          When can I start selling?
        </h2>{" "}
        <p>
          After you complete your business registration, we&#39;ll review your
          information and activate your account within 24 hours or sooner. Once
          activated, your store will be live on the Fine Foods app, and
          customers can see the surplus food available.
        </p>
        <h2 className="text-3xl font-bold sm:text-2xl md:text-3xl  my-3 ">
          What should I do if I have no surplus food?
        </h2>{" "}
        <p>
          No worries! If you don’t have any surplus food, just cancel the Treat
          Bags from the app dashboard. Your customers will get a notification
          ahead of time, so they won’t waste their time or yours coming to the
          store
        </p>
        <h2 className="text-3xl font-bold sm:text-2xl md:text-3xl  my-3 ">
          How does the price in the Fine Foods app differ from original retail
          price of the foods?
        </h2>{" "}
        <p>
          In the Fine Foods Customer App, the price shown is what users pay for
          each Treat Bag. This price is lower than the original retail price of
          the items you include in the Treat Bags. The original retail price
          must always be higher than what customers pay. This helps offer
          surplus food at a lower cost and reduces food waste.
        </p>
      </div>
    </>
  );
};

export default Faq;
