import Navbar from "../components/Navbar";

const Cookiez = () => {
  return (
    <>
     <div className="sticky top-0 z-50 bg-[#00563B]">
          <Navbar />
        </div>
      <div className="p-[50px] md:p-[80px]">
        {/* <h1 className="text-3xl sm:text-2xl md:text-3xl  my-3 ">Cookie Policy</h1> */}

        <h1 className="text-5xl  my-3 text-center mb-4"> Cookie Policy</h1>

        <h1 className="font-bold my-4">Effective Date: 7/10/2024</h1>
        <p>
          At Fine Foods, we use cookies and similar tracking technologies on our
          app and website to provide, enhance, and personalize your experience.
          This Cookie Policy explains what cookies are, how we use them, the
          choices you have regarding them, and how you can manage them. By
          continuing to use our Services, you consent to the use of cookies in
          accordance with this Cookie Policy.
        </p>
        <h2 className="text-3xl sm:text-2xl md:text-3xl  my-3 ">
          1. What Are Cookies?
        </h2>
        <p>
          Cookies are small text files that are stored on your device (computer,
          smartphone, tablet) when you visit a website or use an app. They help
          websites and apps remember your preferences, track your activity, and
          improve your user experience. Cookies may be set by the website or app
          you’re visiting (first-party cookies) or by third parties (third-party
          cookies) that provide services to the site or app.
        </p>

        <h2 className="text-3xl sm:text-2xl md:text-3xl  my-3 ">
          2. How We Use Cookies
        </h2>
        <p>
          Fine Foods uses cookies to enhance the functionality of our Services
          and to provide you with a more personalized experience. Specifically,
          we use cookies for the following purposes:
        </p>

        <h2 className="text-3xl sm:text-2xl md:text-2xl  my-3 ">
          2.1. Essential Cookies:
        </h2>
        <p>
          These cookies are necessary for the proper functioning of our website
          and app. Without them, certain features, such as account login and
          order processing, would not work.
        </p>

        <h2 className="text-3xl sm:text-2xl md:text-2xl  my-3 ">
          2.2. Performance and Analytics Cookies:
        </h2>
        <p>
          We use these cookies to collect information about how users interact
          with our Services, such as which pages are visited most frequently and
          any error messages users may encounter. This helps us improve the
          overall user experience and optimize the performance of our Services.
        </p>

        <h2 className="text-3xl sm:text-2xl md:text-2xl  my-3 ">
          2.3. Functionality Cookies:
        </h2>
        <p>
          These cookies allow our Services to remember your preferences and
          settings (such as language selection, region, or previous purchases)
          to provide a more customized experience.
        </p>

        <h2 className="text-3xl sm:text-2xl md:text-2xl  my-3 ">
          2.4. Targeting and Advertising Cookies:
        </h2>
        <p>
          We may use cookies to deliver personalized advertisements based on
          your browsing behavior. These cookies may be placed by third-party
          advertising networks, and they help us measure the effectiveness of
          our marketing campaigns.
        </p>

        <h2 className="text-3xl sm:text-2xl md:text-2xl  my-3 ">
          2.5. Third-Party Cookies:
        </h2>
        <p>
          We work with third-party services (such as Google Analytics) that may
          place cookies on your device to help us understand how you use our
          Services and to display relevant advertisements. These third parties
          may
        </p>

        <h2 className="text-3xl sm:text-2xl md:text-3xl  my-3 ">
          3. Types of Cookies We Use
        </h2>
        <p>Fine Foods uses both session cookies and persistent cookies:</p>

        <ul className="list-disc ml-6">
          <li>
            <strong>Session Cookies:</strong> These cookies are temporary and
            are deleted once you close your browser or app. They are used to
            maintain your session during your visit.
          </li>
          <li>
            <strong>Persistent Cookies:</strong> These cookies remain on your
            device after you close your browser or app and are used to remember
            your preferences for future visits.
          </li>
        </ul>

        <h2 className="text-3xl sm:text-2xl md:text-3xl  my-3 ">
          4. Your Choices Regarding Cookies
        </h2>

        <p>
          You have the ability to accept or reject cookies through your browser
          or device settings. However, please note that if you choose to block
          or delete cookies, certain features of our Services may not function
          properly, and your experience may be affected.
        </p>

        <h2 className="text-3xl sm:text-2xl md:text-2xl  my-3 ">
          4.1. Browser Settings:
        </h2>
        <p>
          Most web browsers allow you to control cookies through their settings.
          You can typically configure your browser to notify you when a cookie
          is being set, or to block all cookies. The following links provide
          information on how to manage cookies in popular browsers:
        </p>

        <ul className="list-disc ml-6">
          <li>
            <strong>Google Chrome</strong>
          </li>
          <li>
            <strong>Mozilla Firefox</strong>
          </li>

          <li>
            <strong>Apple Safari</strong>
          </li>

          <li>
            <strong>Microsoft Edge</strong>
          </li>
        </ul>

        <h2 className="text-3xl sm:text-2xl md:text-2xl  my-3 ">
          4.3. Opting Out of Targeted Advertising:
        </h2>
        <p>
          Some third-party services, like Google Analytics, allow you to opt out
          of tracking for advertising purposes. You can visit the following
          links to adjust your preferences:
        </p>

        <ul className="list-disc ml-6">
          <li>
            <p>Google Analytics Opt-out</p>
          </li>
          <li>
            <p>Network Advertising Initiative</p>
          </li>
        </ul>

        <h2 className="text-3xl sm:text-2xl md:text-3xl  my-3 ">
          5. Third-Party Cookies and Links
        </h2>
        <p>
          Fine Foods may include links to third-party websites or services that
          have their own cookie policies. We do not control or have
          responsibility over the use of cookies on third-party websites. We
          encourage you to review the privacy and cookie policies of these
          third-party websites for more information.
        </p>

        <h2 className="text-3xl sm:text-2xl md:text-3xl  my-3 ">
          6. Changes to This Cookie Policy
        </h2>
        <p>
          Fine Foods may update this Cookie Policy from time to time to reflect
          changes in our use of cookies or applicable legal requirements. We
          will notify you of any significant changes by posting the updated
          Cookie Policy on our website or app and updating the effective date at
          the top. Your continued use of the Services after such changes
          constitutes your acceptance of the new Cookie Policy.
        </p>

        <h2 className="text-3xl sm:text-2xl md:text-3xl  my-3 ">
          7. Contact Us
        </h2>

        <p>
          If you have any questions about our use of cookies or how we process
          your personal data, please contact us at:
        </p>

        <ul>
          <li>
            <strong>Email:</strong> finelookstech@gmail.com
          </li>
          <li>
            <strong>Phone:</strong> +966596482218
          </li>
        </ul>
        <p className="mt-4">
          Join the Fine Foods community and help us revolutionize the way we
          think about food!
        </p>
      </div>
    </>
  );
};

export default Cookiez;
