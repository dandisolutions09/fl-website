import React from "react";

export default function AccountDeletionPolicy() {
  return (
    <>
      <div className="h-auto">
        <div className="flex flex-col justify-center items-center place-content-center mt-[100px] mb-4">
          <h1 className="text-5xl underline my-3">Account Deletion Policy</h1>
          <h1 className="font-bold my-4">
            Account Deletion Policy - Last updated May 2024
          </h1>
          <div className="px-[400px] text-left">
            <h2>1. Purpose</h2>
            <p>
              The purpose of this Account Deletion Policy is to outline the
              procedures and guidelines for deleting user accounts from our
              system. This policy ensures that user data is managed and deleted
              in a secure, compliant, and consistent manner.
            </p>
            <h2>2. Scope</h2>
            <p>
              This policy applies to all users of our platform, including but
              not limited to customers, employees, contractors, and partners. It
              covers all types of accounts, including user accounts,
              administrative accounts, and any other type of accounts that
              access our system.
            </p>

            <h3>3.3 Processing the Deletion</h3>
            <p>
              Once verified, the account will be marked for deletion. The
              account will be deactivated immediately, preventing further
              access. All data associated with the account will be permanently
              deleted from our servers within 15 days from the request date.
            </p>

  
            <h2>5. User Responsibilities</h2>
            <p>
              Users are responsible for ensuring that they download or transfer
              any data they wish to retain before requesting account deletion.
              Users must notify us promptly if they believe their account
              deletion request has not been processed correctly.
            </p>
            <h2>6. Policy Review</h2>
            <p>
              This policy will be reviewed annually or as needed to ensure
              compliance with legal requirements and best practices.
            </p>
          </div>
        </div>
      </div>
    </>
  );
}
