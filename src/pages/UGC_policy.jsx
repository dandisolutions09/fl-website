import Navbar from "../components/Navbar";

const UserContentPolicy = () => {
  return (
    <>
       <div className="sticky top-0 z-50 bg-[#00563B]">
          <Navbar />
        </div>
      <div className="p-[50px] md:p-[80px]">
        {/* <h1 className="text-3xl sm:text-2xl md:text-3xl  my-3 ">Cookie Policy</h1> */}

        <h1 className="text-5xl  my-3 text-center mb-4">
          {" "}
          User Generated Content Policy
        </h1>

        <h1 className="font-bold my-4">Effective Date: 7/10/2024</h1>
        <p>
          At Fine Foods, we value and encourage our community to share their
          experiences, reviews, and feedback. This User Generated Content (UGC)
          Policy outlines the rules and guidelines for any content (including
          but not limited to text, photos, videos, reviews, comments, or other
          materials) you submit to the Fine Foods app, website, or any of our
          platforms (collectively, the “Services”).
        </p>
        <p>
          By submitting any UGC, you agree to comply with the following terms.
        </p>
        <h2 className="text-3xl sm:text-2xl md:text-3xl  my-3 ">
          1. Acceptance of the UGC Policy
        </h2>
        <p>
          By submitting or posting UGC to Fine Foods, you acknowledge that you
          have read, understood, and agreed to the terms of this UGC Policy. If
          you do not agree with any of the terms, you must refrain from
          submitting UGC.
        </p>

        <h2 className="text-3xl sm:text-2xl md:text-3xl  my-3 ">
          2. Eligibility to Submit Content
        </h2>
        <p>
          You must be 18 years or older to submit UGC to Fine Foods. By
          submitting content, you represent that you meet this age requirement
          and that the content is your own original work or you have the
          necessary rights and permissions to submit the content to us.
        </p>

        <h2 className="text-3xl sm:text-2xl md:text-3xl  my-3 ">
          3. Types of User Generated Content
        </h2>
        <p>UGC includes, but is not limited to:</p>

        <ul className="list-disc ml-6">
          <li>
            Reviews of Treat Bags or experiences with participating businesses.
          </li>
          <li>Photos or videos of food purchased through Fine Foods.</li>
          <li>
            Comments, suggestions, or feedback related to Fine Foods’ services
            or app features.
          </li>
          <li>
            Social media posts or comments related to Fine Foods when you tag us
            or use our official hashtags.
          </li>
        </ul>

        <h2 className="text-3xl sm:text-2xl md:text-3xl  my-3 ">
          4. Content Ownership and License
        </h2>
        <h2 className="text-3xl sm:text-2xl md:text-2xl  my-3 ">
          4.1. Your Ownership:
        </h2>
        <p>
          You retain ownership of any content you create and submit to Fine
          Foods. However, by submitting UGC, you grant Fine Foods a perpetual,
          irrevocable, royalty-free, worldwide, non-exclusive license to use,
          reproduce, distribute, modify, display, perform, and otherwise exploit
          your UGC in connection with the operation of our Services, including
          for promotional and marketing purposes.
        </p>

        <h2 className="text-3xl sm:text-2xl md:text-2xl  my-3 ">
          4.1. Rights Granted:
        </h2>
        <p>By submitting UGC, you grant Fine Foods the right to:</p>

        <ul className="list-disc ml-6">
          <li>
            Display your content on our app, website, social media channels, or
            any marketing materials.
          </li>
          <li>
            Modify or adapt your content for technical, editorial, or
            operational reasons (e.g., cropping an image or resizing it).
          </li>
          <li>
            Use your username, social media handle, or profile name in
            connection with the UGC (with appropriate credit, if applicable).
          </li>
        </ul>

        <h2 className="text-3xl sm:text-2xl md:text-3xl  my-3 ">
          5. Prohibited Content
        </h2>

        <p>You are prohibited from submitting any UGC that:</p>

        <ul className="list-disc ml-6">
          <li>
            Is illegal, harmful, abusive, harassing, defamatory, libelous,
            obscene, vulgar, or otherwise objectionable.
          </li>
          <li>
            Violates the privacy rights, intellectual property rights, or any
            other rights of a third party.
          </li>
          <li>
            Contains any form of hate speech, violence, discrimination, or
            threats.
          </li>

          <li>
            Promotes illegal activity or encourages conduct that would
            constitute a criminal offense.
          </li>

          <li>
            Contains any form of advertising or commercial solicitation, except
            as explicitly permitted.
          </li>

          <li>
            Includes personal data (e.g., addresses, phone numbers) without
            proper consent.
          </li>

          <li>
            Impersonates any person or entity, or falsely states or otherwise
            misrepresents your affiliation with a person or entity.
          </li>
        </ul>

        <h2 className="text-3xl sm:text-2xl md:text-3xl  my-3 ">
          6. Content Moderation
        </h2>

        <h2 className="text-3xl sm:text-2xl md:text-2xl  my-3 ">
          6.1. Monitoring:
        </h2>
        <p>
          While Fine Foods may monitor and review UGC, we are not obligated to
          do so. We reserve the right to remove, edit, or refuse to display any
          UGC at our sole discretion, especially if it violates this UGC Policy,
          our Terms and Conditions, or any applicable laws.
        </p>

        <h2 className="text-3xl sm:text-2xl md:text-2xl  my-3 ">
          6.2. Reporting Violations:
        </h2>
        <p>
          If you believe any UGC violates this Policy or any rights, you can
          report the content by contacting us at  finelookstech@gmail.com.
          Fine Foods will review the complaint and take appropriate action,
          which may include removing the content or disabling the user’s
          account.
        </p>

        <h2 className="text-3xl sm:text-2xl md:text-3xl  my-3 ">
          7. Accuracy and Responsibility for UGC
        </h2>

        <p>
          You are solely responsible for the content you submit. By submitting
          UGC, you represent and warrant that:
        </p>

        <ul className="list-disc ml-6">
          <li>The content is accurate and truthful.</li>
          <li>You have the legal right to submit the content.</li>
          <li>
            Your submission does not violate any laws, this Policy, or any
            third-party rights.
          </li>

          <li>
            The content does not contain any misleading or deceptive
            information.
          </li>
        </ul>

        <p>
          Fine Foods does not endorse any UGC submitted by users and is not
          responsible for the accuracy, reliability, or legality of such
          content.
        </p>

        <h2 className="text-3xl sm:text-2xl md:text-3xl  my-3 ">
          8. Intellectual Property Infringement
        </h2>

        <p>
          Fine Foods respects intellectual property rights and expects users to
          do the same. If you believe that any UGC on the Services infringes
          your intellectual property rights, please notify us in writing with
          all necessary details at  finelookstech@gmail.com. We will
          investigate and take action as necessary, including removing the
          infringing content or terminating the user’s account.
        </p>

        <h2 className="text-3xl sm:text-2xl md:text-3xl  my-3 ">
          9. Termination of Account
        </h2>

        <p>
          Fine Foods reserves the right to terminate or suspend your account if
          you repeatedly violate this UGC Policy or submit content that is
          unlawful or in violation of third-party rights. We may also terminate
          your account if we receive multiple valid complaints regarding your
          UGC.
        </p>

        <h2 className="text-3xl sm:text-2xl md:text-3xl  my-3 ">
          10. Third-Party Platforms
        </h2>

        <p>
          If you submit UGC via third-party platforms (such as social media),
          your content will also be subject to the terms and conditions of those
          platforms. Fine Foods is not responsible for the management or content
          policies of third-party platforms, and we encourage you to review
          their policies before submitting UGC.
        </p>

        <h2 className="text-3xl sm:text-2xl md:text-3xl  my-3 ">
          11. Changes to this UGC Policy
        </h2>

        <p>
          Fine Foods may update or modify this UGC Policy from time to time. We
          will notify users of any significant changes by posting the updated
          policy on our website or app. Your continued use of the Services after
          any changes constitutes acceptance of the new policy.
        </p>

        <h2 className="text-3xl sm:text-2xl md:text-3xl  my-3 ">
          12. Contact Us
        </h2>

        <p>
          If you have any questions, concerns, or feedback regarding this UGC
          Policy, or if you would like to report a violation, please contact us
          at:
        </p>

        <ul>
          <li>
            <strong>Email:</strong> finelookstech@gmail.com
          </li>
          <li>
            <strong>Phone:</strong> +966596482218
          </li>
        </ul>
      </div>
    </>
  );
};

export default UserContentPolicy;
