// import React from "react";
// import img2 from "../assets/ios.png";
// import img3 from "../assets/android.png";
// import { MdCheckBoxOutlineBlank } from "react-icons/md";
// import { NavLink } from "react-router-dom";
// import { FaFacebook, FaTwitter } from "react-icons/fa6";
// import { FaInstagramSquare } from "react-icons/fa";

// export default function Footer() {
//   return (
//     <>
//       <div className="flex flex-col bg-[#00563B] h-auto ">
//         <div className="flex flex-col sm:flex-row gap-12 items-center justify-around py-6">
//           {/* Icons section */}

//           {/* Links Section 1 */}
//           <div className="text-white text-sm flex flex-row gap-3">
//             <NavLink
//               to="/about"
//               className="cursor-pointer hover:scale-110 duration-200 ease-in"
//             >
//               About Us
//             </NavLink>
//             <NavLink
//               to="/user-content"
//               className="cursor-pointer hover:scale-110 duration-200 ease-in"
//             >
//               User Generated Content Policy
//             </NavLink>
//             <NavLink
//               to="/terms"
//               className="cursor-pointer hover:scale-110 duration-200 ease-in"
//             >
//               Terms & Conditions
//             </NavLink>
//             {/* <NavLink
//               to="/privacy"
//               className="cursor-pointer hover:scale-110 duration-200 ease-in"
//             >
//               Privacy Policy
//             </NavLink> */}
//             {/* <NavLink
//               to="/account-deletion"
//               className="cursor-pointer hover:scale-110 duration-200 ease-in"
//             >
//               Account Deletion Policy
//             </NavLink> */}

//             <NavLink
//               to="/cookie-policy"
//               className="cursor-pointer hover:scale-110 duration-200 ease-in"
//             >
//               Cookie Policy
//             </NavLink>

//             <NavLink
//               to="/user-content"
//               className="cursor-pointer hover:scale-110 duration-200 ease-in"
//             >
//               User Generated content policy
//             </NavLink>
//           </div>

//           {/* App Download Section */}
//         </div>
//         {/* Centering App Images */}
//         <div className="flex justify-center gap-6 py-4">
//           <div>
//             <img
//               src={img2}
//               alt="iOS App"
//               width={130}
//               className="cursor-pointer hover:scale-110 duration-200 ease-in"
//             />
//           </div>
//           <div>
//             <img
//               src={img3}
//               alt="Android App"
//               width={130}
//               className="cursor-pointer hover:scale-110 duration-200 ease-in"
//             />
//           </div>
//         </div>

//         <div className="flex justify-center gap-6 py-4">
//           <div>
//             <FaFacebook size={50}/>
//           </div>

//           <div>
//             <FaInstagramSquare size={50}/>
//           </div>

//           <div>
//             <FaTwitter size={50}/>
//           </div>

//         </div>

//         {/* Copyright section */}
//         <h1 className="font-thin text-center text-[#f1f1f1] text-xs py-6">
//           © Copyright 2024 @FineLooks
//         </h1>
//       </div>
//     </>
//   );
// }

import React from "react";
import img2 from "../assets/ios.png";
import img3 from "../assets/android.png";
import { MdCheckBoxOutlineBlank } from "react-icons/md";
import { NavLink } from "react-router-dom";
import { FaFacebook, FaTwitter } from "react-icons/fa6";
import { FaInstagramSquare } from "react-icons/fa";

export default function Footer() {
  return (
    <>
      <div className="flex flex-col bg-[#00563B] h-auto">
        {/* Links and App Download Section */}
        <div className="flex flex-col md:flex-row gap-8 items-center justify-between px-4 sm:px-6 md:px-12 lg:px-16 py-6">
          {/* Links Section */}
          <div className="text-white text-xs sm:text-sm md:text-base flex flex-col sm:flex-row gap-3 sm:gap-6">
            <NavLink
              to="/about"
              className="cursor-pointer hover:scale-110 duration-200 ease-in"
            >
              About Us
            </NavLink>
            <NavLink
              to="/user-content"
              className="cursor-pointer hover:scale-110 duration-200 ease-in"
            >
              User Generated Content Policy
            </NavLink>
            <NavLink
              to="/terms"
              className="cursor-pointer hover:scale-110 duration-200 ease-in"
            >
              Terms & Conditions
            </NavLink>
            <NavLink
              to="/cookie-policy"
              className="cursor-pointer hover:scale-110 duration-200 ease-in"
            >
              Cookie Policy
            </NavLink>

            <NavLink
              to="/privacy"
              className="cursor-pointer hover:scale-110 duration-200 ease-in"
            >
              Privacy Policy
            </NavLink>


            <NavLink
              to="/delete-partner"
              className="cursor-pointer hover:scale-110 duration-200 ease-in"
            >
              Delete Partner Account
            </NavLink>


            <NavLink
              to="/delete-customer"
              className="cursor-pointer hover:scale-110 duration-200 ease-in"
            >
              Delete Customer Account
            </NavLink>
          </div>

          {/* App Download Section */}
          <div className="flex gap-4">
            <div>
              <a
                href="https://apps.apple.com/app/id1234567890"
                target="_blank"
                rel="noopener noreferrer"
              >
                <img
                  src={img2}
                  alt="iOS App"
                  width={130}
                  className="cursor-pointer hover:scale-110 duration-200 ease-in"
                />
              </a>
            </div>
            <div>
              <a
                href="https://play.google.com/store/apps/details?id=com.finelooks.finefoods&pcampaignid=web_share"
                target="_blank"
                rel="noopener noreferrer"
              >
                <img
                  src={img3}
                  alt="Android App"
                  width={130}
                  className="cursor-pointer hover:scale-110 duration-200 ease-in"
                />
              </a>
            </div>
          </div>
        </div>

        {/* Social Media Section */}
        <div className="flex justify-center gap-6 py-4">
          <div>
            <FaFacebook
              size={40}
              className="text-white cursor-pointer hover:scale-110 duration-200 ease-in"
            />
          </div>
          <div>
            <FaInstagramSquare
              size={40}
              className="text-white cursor-pointer hover:scale-110 duration-200 ease-in"
            />
          </div>
          <div>
            <FaTwitter
              size={40}
              className="text-white cursor-pointer hover:scale-110 duration-200 ease-in"
            />
          </div>
        </div>

        {/* Copyright Section */}
        <h1 className="font-thin text-center text-[#f1f1f1] text-xs py-4">
          © Copyright 2024 @FineLooks
        </h1>
      </div>
    </>
  );
}
