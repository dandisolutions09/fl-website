import React from "react";
import HomePage from "./pages/HomePage";
import AboutPage from "./pages/AboutPage";
import ContactPage from "./pages/ContactPage";
import { Routes, Route, HashRouter } from "react-router-dom";
import Navbar from "./components/Navbar";
import "@fontsource/roboto/300.css";
import "@fontsource/roboto/400.css";
import "@fontsource/roboto/500.css";
import "@fontsource/roboto/700.css";
import Footer from "./pages/Footer";
// import PrivacyPolicy from './pages/PrivacyPolicy'
import TermsAndCondition from "./pages/TermsAndCondition";
import Usersguide from "./pages/Usersguide";
import AccountDeletionPolicy from "./pages/AccountDeletionPolicy";
// import PrivacyPolicy1 from "./pages/PrivacyPolicy";
import PrivacyPol from "./pages/Privacy";
import Cookiez from "./pages/Cookie";
import UserContentPolicy from "./pages/UGC_policy";
import Faq from "./pages/faq";
import DeleteCustomerAccount from "./pages/DeleteCustomerAccount";
import DeletePartnerAccount from "./pages/DeletePartnerAccount";
// import CookieComponent from "./pages/CookiePolicy";
// import { Cookie } from "@mui/icons-material";
// import CookiePolicy from "./pages/CookiePolicy";
// import PrivacyPolicy1 from "./pages/PrivacyPolicy";

export default function App() {
  return (
    <>
      <HashRouter>
        <Routes>
          <Route path="/" element={<HomePage />} />
          <Route path="/contact" element={<ContactPage />} />
          <Route path="/about" element={<AboutPage />} />
          <Route path="/privacy" element={<PrivacyPol />} />
          <Route path="/terms" element={<TermsAndCondition />} />
          {/* <Route path="/userguide" element={<Usersguide />} /> */}
          <Route path="/account-deletion" element={<AccountDeletionPolicy />} />
          {/* <Route path="/cookie-policy" element={<AccountDeletionPolicy />} /> */}
          <Route path="/cookie-policy" element={<Cookiez />} />
          <Route path="/user-content" element={<UserContentPolicy />} />

          <Route path="/faq" element={<Faq />} />

          <Route path="/delete-customer" element={<DeleteCustomerAccount />} />

          <Route path="/delete-partner" element={<DeletePartnerAccount />} />


          
        </Routes>
        <Footer />
      </HashRouter>
    </>
  );
}
